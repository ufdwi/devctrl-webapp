import {Component, Input} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { MatDialogRef } from '@angular/material';


@Component({
    selector: 'devctrl-alert-dialog',
    template: `
<div>
    <label>{{title}}</label>
    <div>
        {{content}}
    </div>
    <button mat-button (click)="dialogRef.close()">{{ok}}</button>
<div>
`
})
export class AlertDialog
{
    title;
    content;
    ok;

    constructor(public dialogRef: MatDialogRef<AlertDialog>) {}


}