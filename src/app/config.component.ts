import {Component, Input} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
    selector: 'devctrl-config',
    template: `
<div *ngIf="noActivatedChildren()">
    <mat-card>
        <mat-card-content>
            <mat-list>
                <a mat-list-item (click)="router.navigate(['config','data'])">
                    Table Data
                    <span flex></span>
                    <mat-icon>chevron_right</mat-icon>
                </a>
                <a mat-list-item (click)="router.navigate(['config','log'])">
                    Logs
                    <span flex></span>
                    <mat-icon>chevron_right</mat-icon>
                </a>
            </mat-list>
        </mat-card-content>
    </mat-card>
</div>
<router-outlet></router-outlet>    
`
})
export class ConfigComponent {
    constructor(private route : ActivatedRoute, private router: Router) {

    }

    noActivatedChildren() {
        let val = this.route.children.length == 0;
        return val;
    }
}