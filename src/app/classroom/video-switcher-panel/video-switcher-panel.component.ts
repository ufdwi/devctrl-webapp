import {ControlService} from "../../controls/control.service";
import {Component, OnInit, Input, ViewChild} from "@angular/core";
import {RoomConfig} from "../RoomConfig";

@Component({
  selector: 'devctrl-video-switcher-panel',
  providers: [ControlService],
  templateUrl: './video-switcher-panel.html',
  styleUrls:['./video-switcher-panel.css']
})
export class VideoSwitcherPanelComponent implements OnInit {
  @Input() roomConfig: RoomConfig;
  @Input() layout = 1;
  configOK = false;
  configErrors = [];
  viewControlIds = [];
  @ViewChild('zg0') zoomGrid0;
  @ViewChild('zg1') zoomGrid1;
  @ViewChild('zg2') zoomGrid2;

  zgState = {
    hidden: true,
    elect: -1,
    current: -1,
    transitionDest: -1,
    disabled: false
  };

  zgRects = [];



  constructor(private controlService: ControlService) {}

  ngOnInit() {
    if (! this.roomConfig.videoSwitcherControlId) {
      this.configErrors.push("videoSwitcherControlId must be set");
    }

    if (this.configErrors.length === 0) {
      this.configOK = true;
    }

    this.viewControlIds = this.roomConfig.videoSwitcherInputViewControlIds;
  }

  imgError($event) {
    console.log("error loading camera view");
    console.log($event);
  }

  /**
   *
   * @param xy {x,y}
   * @return Index of first containing rect from zgRects
   */
  nearestZoomGrid(xy) {
    this.zgRects = [this.zoomGrid0, this.zoomGrid1, this.zoomGrid2]
      .map(zg => zg.nativeElement.getBoundingClientRect() );

    return this.zgRects.findIndex(rect => {
      return rect.left <= xy.x && xy.x <= rect.right && rect.top <= xy.y && xy.y <= rect.bottom;
    })
  }

  onPanOutput(evt) {
    //console.log(`swipe to the ${evt.direction}`);
    this.zgState.elect = this.nearestZoomGrid(evt.center);
  }

  onPanStart(evt) {
    if (this.zgActiveControl()) {
      console.log("zoom grid activated");
      this.zgState.hidden = false;
      this.zgState.transitionDest = -1;
      this.zgState.current = this.zgSelected();
      this.zgState.elect = -1;
    }
    else {
      this.zgState.disabled = true;
    }
  }

  onPanEnd(evt) {
    this.zgState.elect = this.nearestZoomGrid(evt.center);

    let activeInput = this.outputSource();
    let control = this.zgActiveControl();
    if (control
      && this.roomConfig.videoSwitcherZoomGridPresets
      && this.roomConfig.videoSwitcherZoomGridPresets[activeInput]) {
      let presets = this.roomConfig.videoSwitcherZoomGridPresets[activeInput];

      if (typeof presets[this.zgState.elect] !== 'undefined') {
        this.controlService.setValue(presets[this.zgState.elect], control.id);
        this.zgState.transitionDest = this.zgState.elect;
        this.zgState.elect = -1;
      }
    }

    this.zgState.hidden = true;
    this.zgState.disabled = false;

  }

  /**
   * Return the index of of the currently selected input
   */
  outputSource(): string {
    let selectedSourceVal = this.controlService.getControl(this.roomConfig.videoSwitcherControlId).value;

    for (let idx in this.roomConfig.videoSwitcherInputNumbers) {
      if (this.roomConfig.videoSwitcherInputNumbers[idx] == selectedSourceVal) {
        return idx;
      }
    }

    return "-1";
  }

  sourceSelected(idx: number): boolean {
    let val = this.roomConfig.videoSwitcherInputNumbers[idx];
    let cVal = this.controlService.getControl(this.roomConfig.videoSwitcherControlId).value;
    return  cVal == val;
  }


  selectSource(idx: number) {
    let val = this.roomConfig.videoSwitcherInputNumbers[idx];
    this.controlService.setValue(val, this.roomConfig.videoSwitcherControlId);
  }

  videoSource(idx: number) {
    // If cameraViewUrl is defined for idx, use that
   // if (this.roomConfig.cameraViewUrls[idx]) return this.rc.cameraViewUrls[idx];

    const inputIds = this.roomConfig.videoSwitcherInputViewControlIds;

    if (! inputIds[idx]) {
      console.log(`no input view definition found for input ${idx}`);
      return "";
    }

    let control = this.controlService.getControl(inputIds[idx]);

    if (! control.dataLoaded) {
      return "";
    }

    if (control.config["url"]) {
      return control.config["url"];
    }

    let path = control.config["path"];
    let proto = control.config["proto"];
    proto = proto ? proto : "https://";

    if (proto == "http" || proto == "https") {
      proto = proto + "://";
    }

    let host = control.config["host"];
    host = host ? host : control.endpoint.address;

    return proto + host + path;
  }

  zgActiveControl() {
    let activeInput = this.outputSource();

    if (this.roomConfig.videoSwitcherZoomGridControlIds
      && this.roomConfig.videoSwitcherZoomGridControlIds[activeInput]) {
      let cid = this.roomConfig.videoSwitcherZoomGridControlIds[activeInput];
      let control = this.controlService.getControl(cid);

      return control;
    }
  }

  zgClasses(idx) {
    let sizeClasses = ['zg0', 'zg1', 'zg2'];
    let classes = [sizeClasses[idx]];

    if (this.zgState.hidden) {
      classes.push("zg-hidden");
    } else if (this.zgState.current === idx) {
      classes.push('zg-current');
    } else if (this.zgState.elect === idx) {
      classes.push('zg-elect');
    } else {
      classes.push('zg-unselected');
    }

    if (this.zgState.transitionDest > -1) {
      if (this.zgState.current === idx) {
        classes[0] = sizeClasses[this.zgState.transitionDest];
        classes[1] = "zgTransitionSource";
      } else if (this.zgState.transitionDest === idx) {
        classes[1] = "zgTransitionDest";
      }
    }



    return classes.join(' ');

  }


  zgSelected() {
    let activeInput = this.outputSource();
    let control = this.zgActiveControl();

    if (control
      && this.roomConfig.videoSwitcherZoomGridPresets
      && this.roomConfig.videoSwitcherZoomGridPresets[activeInput]) {
      let presets = this.roomConfig.videoSwitcherZoomGridPresets[activeInput];

      return presets.findIndex( val => val == control.value);
    }
  }



}
