import { Component, OnInit, Inject, Input, Output, EventEmitter } from '@angular/core';
import {ControlService} from "../../controls/control.service";
import {interval, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Room} from "@devctrl/common";
import {DataService} from "../../data.service";
import {PanelDialogService} from "../../rooms/panel-dialog.service";
import {RecordEditorService} from "../../data-editor/record-editor.service";
import {RoomConfig} from "../RoomConfig";

@Component({
  selector: 'classroom-control-drawer',
    providers: [
        {
            provide: 'controlServiceAny',
            useClass: ControlService
        },
        {
            provide: 'controlServiceSource',
            useClass: ControlService
        },
        {
            provide: 'controlServiceAudioOn',
            useClass: ControlService
        },
        {
            provide: 'controlServiceVolume',
            useClass: ControlService
        }
    ],
  templateUrl: './control-drawer.component.html',
  styleUrls: ['./control-drawer.component.css']
})
export class ControlDrawerComponent implements OnInit {
    csAny : ControlService;

    @Input() timecode : Observable<string>;
    @Input() room : Room;
    @Input() rc : RoomConfig;
    @Output() close = new EventEmitter<string>();

    constructor(
        @Inject('controlServiceAny') csAny : ControlService,
        private recordService : RecordEditorService,
        private panelService : PanelDialogService
    ) {
      this.csAny = csAny;
    }

    ngOnInit() {

    }

    get audioOnColor() {
        if (this.audioOnValue) {
            return "primary";
        }

        return '';
    }


    audioOnToggle() {
        if (! this.csAny.getControl(this.rc.audioOnControlId)) {
            return;
        }

        this.csAny.setValue(! this.csAny.getControl(this.rc.audioOnControlId).value, this.rc.audioOnControlId);
    }

    get audioOnValue() {
        if (! this.csAny.getControl(this.rc.audioOnControlId)) {
            return false;
        }

        let cVal = this.csAny.getControl(this.rc.audioOnControlId).value;


        return   this.rc.audioOnInverted ? !cVal : cVal;
    }


    editRoom($event) {
        this.recordService.editRecord($event, this.room._id, Room.tableStr);
    }

    openRecordImagePanel($event) {
        if (this.rc.recordViewUrl) {
            this.panelService.openImageDialog($event, "Recorder View",this.rc.recordViewUrl);
        }
    }

    openMorePanel($event) {
        this.panelService.openPanelGroup($event, "_classroom_fullscreen", this.room._id);
    }

    public powerOff() {
        for (let id of this.rc.powerControlIds) {
            this.csAny.setValue(false, id);
        }
    }

    public powerOffColor() {
        if (this.powerStatus() === false) return "primary";

        return "";
    }

    public powerOn() {
        for (let id of this.rc.powerControlIds) {
            this.csAny.setValue(true, id);
        }
    }

    public powerOnColor() {
        if (this.powerStatus()) return "primary";

        return "";
    }

    public powerStatus() {
        let allTrue = true;
        let allFalse = true;

        try {
          for (let id of this.rc.powerControlIds) {
            allTrue = allTrue && this.csAny.getControl(id).value;
            allFalse = allFalse && (this.csAny.getControl(id).value === false);
          }
        }
        catch(e) {
            console.error(e);
            return false;
        }

        if (allTrue) return true;
        if (allFalse) return false;
        return null;
    }

    recordButtonColor() {
        return this.recordStatus() ? "accent" : "";
    }

    recordStatus() {
        let allTrue = true;
        let allFalse = true;

        for (let id of this.rc.recordControlIds) {
            allTrue = allTrue && this.csAny.getControl(id).value;
            allFalse = allFalse && (this.csAny.getControl(id).value === false);
        }

        if (allTrue) return true;
        if (allFalse) return false;
        return null;
    }

    recordToggle() {
        let val = !this.recordStatus()

        for (let id of this.rc.recordControlIds) {
            this.csAny.setValue(val, id);
        }

        if (this.rc.recordTsControlId) {
            // Immediately update the timecode value when a recording is started/stopped
            let tcVal = val ? 1 : 0;
            this.csAny.getControl(this.rc.recordTsControlId).value = tcVal;
        }

    }

    public shutterOff() {
        for (let id of this.rc.shutterControlIds) {
            this.csAny.setValue(false, id);
        }
    }

    public shutterOffColor() {
        if (this.shutterStatus() === false) return "primary";

        return "";
    }

    public shutterOn() {
        for (let id of this.rc.shutterControlIds) {
            this.csAny.setValue(true, id);
        }
    }

    public shutterOnColor() {
        if (this.shutterStatus()) return "primary";

        return "";
    }

    public shutterStatus() {
        let allTrue = true;
        let allFalse = true;

        try {
          for (let id of this.rc.shutterControlIds) {
            allTrue = allTrue && this.csAny.getControl(id).value;
            allFalse = allFalse && (this.csAny.getControl(id).value === false);
          }
        }
        catch (e) {
            console.error(e);
            return false;
        }

        if (allTrue) return true;
        if (allFalse) return false;
        return null;
    }



    public sourceColor(opt) {
        let control;
        if (! (control = this.csAny.getControl(this.rc.sourceControlId))) {
            return '';
        }

        if (control.value == opt) {
            return "primary";
        }

        return '';
    }


    public sourceSelect(opt) {
        this.csAny.setValue(opt, this.rc.sourceControlId);
    }


    public updateVolume(val) {
        this.csAny.setValue(val, this.rc.audioVolumeControlId);

        // Setting the volume unmutes the channel
        if (! this.csAny.getControl(this.rc.audioVolumeControlId).value) {
            this.csAny.setValue(true, this.rc.audioVolumeControlId);
        }
    }


    get volumeDisplay() {
        return this.volumeDisplayValue(this.volumeVal());
    }



    public volumeDisplayValue(val) {
        if (val >= 423) {
            val = -20 + 0.05 * (val - 423);
        }
        else if (val >= 223) {
            val = -40 + 0.10 * (val - 223);
        }
        else if (val >= 33 ) {
            val = -78 + 0.20 * (val - 33);
        }
        else if (val >= 15 ) {
            val = -96 + (val - 15);
        }
        else if (val >= 1) {
            val = -133 + 3 * (val - 1);
        }
        else {
            return "-INF";
        }

        let strVal = val.toString();
        if (val < -100) strVal = strVal.substring(0,4);
        else if (val < -10) strVal = strVal.substring(0,3);
        else if (val < 0) strVal = strVal.substring(0, 4);
        else if (val > 0) strVal = strVal.substring(0, 3);
        return strVal;
    }

    public volumePreset(preset) {
      let presets = this.rc.volumePresets;

      this.updateVolume(presets[preset]);
    }

    public volumeVal() {
        if (this.audioOnValue === false) {
            return 0;
        }
        let control = this.csAny.getControl(this.rc.audioVolumeControlId);

        let val = control.value;
        return val;
    }
}
