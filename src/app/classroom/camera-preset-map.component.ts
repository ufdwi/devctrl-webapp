import {Component, Input, OnInit, Inject, Output, EventEmitter} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { MatDialog } from '@angular/material';
import {ControlService} from "../controls/control.service";
import {Control} from "@devctrl/common";
import {DataService} from "../data.service";
import {RecordEditorService} from "../data-editor/record-editor.service";
import {StudentNameEditorComponent} from "./student-name-editor.component";
import {ICourseInfo, IDCCourseMeeting} from "./course-schedule.service";
import {Observable} from "rxjs";

@Component({
  selector: 'devctrl-camera-preset-map',
  providers: [
    {
      provide: 'controlService',
      useClass: ControlService
    }
  ],
  template: `
    <div class="class-section fullscreen-preset-map">
        <div class="class-left">
            <div *ngFor="let row of classRows"
                    class="class-row">
                <div *ngFor="let seat of row" class="seat">
                    <button mat-raised-button (click)="presetSelected(seat)"
                            (onLongPress)="openRosterDrawer.emit()" long-press
                            [color]="buttonColor(seat)"
                            *ngIf="seat" cdkDropList (cdkDropListDropped)="nameDropped($event, seat)" >
                        <div class="button-contents">
                            <div class="button-number"
                                 [class.button-number-big]="! buttonName(seat, 'first')">
                                {{seat}}
                            </div>
                            <div class="button-name"  cdkDrag
                             [cdkDragData]="{ name: buttonName(seat, 'full'), seat: seat, controlId: cs.controlId }">
                              <div class="button-firstname"
                                [class.button-long-firstname]="buttonLongFirstname(seat)">
                              {{buttonName(seat, 'first')}}
                              </div>
                              <div class="button-lastname">{{buttonName(seat, 'last')}}</div>
                            </div>
                        </div>
                    </button>
                </div>
            </div>
            <div class="class-row class-row-bottom wide-button" [ngClass]="bottomRowClassMap">
                <div class="bottom-row-spacer">&nbsp;</div>
                <button mat-raised-button (click)="presetSelected(0)" [color]="buttonColor(0)">Wide</button>
                <div class="settings-area">
                    <button mat-button *ngIf="fullscreenButton && ! isFullscreen()"
                            (click)="requestFullscreen()" class="fullscreen-button">
                        <mat-icon aria-label="Fullscreen" class="fullscreen-icon">fullscreen</mat-icon>
                    </button>
                    <button mat-button *ngIf="fullscreenButton && isFullscreen()"
                            (click)="exitFullscreen()" class="fullscreen-button">
                        <mat-icon aria-label="Exit Fullscreen" class="fullscreen-icon">fullscreen_exit</mat-icon>
                    </button>
                </div>
            </div>
        </div>
    </div>
`,
  styles:
  //language=CSS
    [`
      .bottom-row-spacer {
        flex: 1 1;
      }

      .br-l1 .settings-area {
        flex: 1.7;    
      }
      
      .br-r1 .bottom-row-spacer {
          flex: 3.8;
      }

      .button-contents {
        display: flex;
        flex-direction: column;
      }

      .button-lastname {
        flex: .8 .8;
        font-size: 0.8vw
      }

      .button-firstname {
        flex: 1 1;
        font-size: 1.1vw;
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;
        line-height: 1.8vw;
      }

      .button-firstname.button-long-firstname {
        font-size: 0.7vw;
      }

      .button-number {
        font-size: 0.7vw;
      }

      .button-number.button-number-big {
        font-size: 1.2vw;
      }


      .class-section {
        width: 100%;
        height: 100%;
        display: flex;
        flex-direction: row;
      }

      .class-left {
        display: flex;
        flex-direction: column;
        flex: 5 5;
      }

      .class-center-aisle {
        flex: 1 1;
        display: flex;
        flex-direction: column;
        justify-content: flex-end;
        align-items: center;
      }

      .class-row {
        display: flex;
        flex-direction: row;
        flex: 1 1;
      }

      .class-row-bottom {
        justify-content: center;
        align-items: center;
      }


      .fullscreen-icon {
        font-size: 2.5vw;
        height: 3vw;
        width: 3vw;
      }

      .seat {
        flex: 1 1;
        display: flex;
        justify-content: center;
        align-items: center;
      }


      .settings-area {
        flex: 1 1;
        display: flex;
        flex-direction: row;
        justify-content: flex-end;
        align-items: flex-end;
        height: 100%;
      }
    `]
})
export class CameraPresetMapComponent implements OnInit
{
  cs : ControlService;
  @Input() course : Observable<IDCCourseMeeting>;
  @Input() classRows : number[][];
  @Input() bottomRowStyle = "default";
  @Input() fullscreenButton = true;
  @Output() openRosterDrawer = new EventEmitter();
  courseCode;
  sectionCode = '';
  instructor = '';
  bottomRowClassMap = {};

  constructor(
    private ds: DataService,
    @Inject('controlService') cs : ControlService,
    private mdDialog: MatDialog
  ) {
    this.cs = cs;
  }

  ngOnInit() {
    this.course.subscribe(course => {
      this.courseCode = course.courseCode;
      this.sectionCode = course.section;
    });

    this.bottomRowClassMap = {
      'br-default': this.bottomRowStyle == 'default',
      'br-l1': this.bottomRowStyle == 'l1',
      'br-r1': this.bottomRowStyle == 'r1'
    }
    //window.addEventListener("contextmenu", (e) => { e.preventDefault()});
  }


  buttonColor(seat: number) {
    if (! this.cs.control) return '';
    if (this.cs.value == seat) return 'primary';

    return '';
  }

  buttonName(seat: number, firstLast: string) {
    if (! this.cs.control) {
      return '';
    }
    let cs = this.cs;

    let names;
    if (names = cs.config("names")) {
      if (names[this.courseCode] && names[this.courseCode][this.sectionCode]
        && names[this.courseCode][this.sectionCode][seat]) {
        let fullname = names[this.courseCode][this.sectionCode][seat];
        let components = fullname.split(",");
        if (firstLast == "first" && components[1]) {
          return components[1];
        }
        else if (firstLast == "last") {
          return components[0];
        }
        else if (firstLast == "full") {
          return fullname;
        }

      }
    }

    return '';
  }


  buttonLongFirstname(seat: number) {
    let name = this.buttonName(seat, "first");
    if (name.length > 11) {
      return true;
    }

    return false;
  }


  editName(event, seat: number, section: string) {
    console.log(`edit seat name ${seat}${section}`);
    let nameRef = this.mdDialog.open(StudentNameEditorComponent);
    nameRef.componentInstance.course = this.courseCode;
    nameRef.componentInstance.section = this.sectionCode;
    nameRef.componentInstance.name = this.buttonName(seat, "full");
    nameRef.componentInstance.seat = seat;
    nameRef.componentInstance.control = this.cs.control;
  }


  exitFullscreen() {
    if (document.exitFullscreen) {
      document.exitFullscreen();
    }
    else if (document.exitFullscreen) {
      document.exitFullscreen();
    }
  }

  isFullscreen() {
    return !! (<any>document).fullscreenElement;
  }

  @Input() set controlId(controlId: string) {
    this.cs.controlId = controlId;
  }

  nameDropped($event, seat: number) {
    console.log(`item dropped`);
    console.dir($event);
    if ($event.item.data.seat) {
        // Unassign student from previous seat
      this.updateStudentName($event.item.data.seat, '');
    }

    this.updateStudentName(seat, $event.item.data.name);
  }

  presetSelected(seat: number) {
    if (this.cs) {
      this.cs.setValue(seat);
    }
    else {
      console.error("presetSelected: control service not set ");
    }

  }



  requestFullscreen() {
    let dl = document.documentElement as any;

    if (dl.requestFullscreen) {
      dl.requestFullscreen();
    }
    else if (dl.mozRequestFullScreen) {
      dl.mozRequestFullScreen();
    }
    else if (dl.webkitRequestFullScreen) {
      dl.webkitRequestFullScreen();
    }
  }

  updateStudentName(seat: number, name: string) {
      this.ds.updateStudentNameConfig(this.cs.control.id,
        this.courseCode,
        this.sectionCode,
        seat,
        name);
  }
}
