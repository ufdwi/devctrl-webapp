import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import {IDCCourseMeeting} from "../course-schedule.service";
import {Observable, interval} from 'rxjs';
import {map} from 'rxjs/operators';
import {RoomConfig} from "../RoomConfig";

@Component({
  selector: 'devctrl-class-info',
  templateUrl: './class-info.component.html',
  styleUrls: ['./class-info.component.css']
})
export class ClassInfoComponent implements OnInit {

  @Input() layout = "narrow";
  @Input() courseList: IDCCourseMeeting[];
  @Input() nextCourseMeeting : Observable<IDCCourseMeeting>;
  @Input() rc: RoomConfig;
  @Output() selectedCourse = new EventEmitter<string>();

  timeRemaining : Observable<ITimeRemaining>;
  currentTime = interval(1000).pipe(map(() => new Date()));

  constructor() { }

  ngOnInit() {
    this.timeRemaining = this.nextCourseMeeting.pipe(map(cm => {
      let untilStart = cm.msUntilStart;
      let untilEnd = cm.msUntilEnd;

      let ret: ITimeRemaining = {
        HH: "HH",
        MM: "MM",
        caption: "REMAINING",
        sign: "",
        timeStr: "00:00"
      };

      let remainingMs = untilEnd;

      if (untilStart < untilEnd) {
        remainingMs = untilStart;
        ret.caption = "BEGINS IN"
      }

      if (remainingMs < 0) {
        ret.sign = "-";
        remainingMs =  0 - remainingMs;  // Use the positive number to calculate the minutes and seconds
      }

      let hourMs = 3600 * 1000;
      let remainingHours = Math.floor(remainingMs / hourMs);
      let minuteRemainder = remainingMs - remainingHours * hourMs;
      let minutes = Math.floor(minuteRemainder / 60000);
      let secondRemainder = minuteRemainder - minutes * 60000;
      let seconds = Math.floor(secondRemainder / 1000);
      let sec = ("00" + seconds).slice(-2);

      let hours = ("00" + remainingHours).slice(-2);
      let min = ("00" + minutes).slice(-2);

      if (remainingMs < 5 * 60 * 1000) {
        // Count down the final 5 minutes by the second
        ret.HH = "MM";
        ret.MM = "SS";
        ret.timeStr = `${min}:${sec}`;
      }
      else {
        ret.timeStr = `${hours}:${min}`;
      }

      return ret;
    }));
  }


  onPinch(evt) {
    console.log("we pinching now!");
  }

  setSelectedCourse(course) {
    this.selectedCourse.emit(course);
  }

}


interface ITimeRemaining {
  HH: string;
  MM: string;
  sign: string;
  caption: string;
  timeStr: string;
}
