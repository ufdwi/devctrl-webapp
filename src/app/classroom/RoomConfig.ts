export class RoomConfig {
    cameraViewControlIds: string[] = [];
    cameraViewUrls: string[] = [];
    presetMapControlIds: string[] = [];
    classMaps: number[][][] = [];  // [section][row][preset number]
    sourceControlId = '';
    audioOnControlId =  '';
    audioOnInverted = false; // Set to true if audioOn control is a mute control
    audioVolumeControlId = '';
    powerControlIds = [];
    recordControlIds = [];
    recordTsControlId = '';
    recordViewUrl = '';
    shutterControlIds = [];
    roomNumber = '';
    clockColor: string = "#3F51B5";
    classClockColor: string = "#FF0000";
    timecodeColor: string = "#3F51B5";

    videoSwitcherLayout = 1;
    videoSwitcherControlId = '';
    videoSwitcherOutputViewUrl = '';
    videoSwitcherInputNumbers: number[] = [];
    videoSwitcherInputViewControlIds: string[] = [];

    videoSwitcherZoomGridControlIds: string[] = [];
    videoSwitcherZoomGridPresets: number[][] = [];  // [input][preset number]

    volumePresets: number[] = [400, 600, 800, 1000];

    constructor() {}
}
