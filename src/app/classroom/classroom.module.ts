import {DCMaterialModule} from "../dc-material.module";
import { RouterModule } from '@angular/router';
import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {ClassroomFullscreenComponent} from "./classroom-fullscreen/classroom-fullscreen.component";
import {CameraPresetMapComponent} from "./camera-preset-map.component";
import {LongPressDirective} from "./long-press.directive";
import {StudentNameEditorComponent} from "./student-name-editor.component";
import {CourseScheduleService} from "./course-schedule.service";
import {ControlDrawerComponent} from "./control-drawer/control-drawer.component";
import {DevctrlCommonModule} from "../devctrl-common.module";
import {VideoSwitcherPanelComponent} from "./video-switcher-panel/video-switcher-panel.component";
import {ClassInfoComponent} from "./class-info/class-info.component";
import {RosterDrawerComponent} from "./roster-drawer/roster-drawer.component";


@NgModule({
  imports: [
    DCMaterialModule,
    FormsModule,
    CommonModule,
    DevctrlCommonModule,
    RouterModule
  ],
  declarations: [
    ClassInfoComponent,
    ClassroomFullscreenComponent,
    CameraPresetMapComponent,
    LongPressDirective,
    RosterDrawerComponent,
    StudentNameEditorComponent,
    ControlDrawerComponent,
    VideoSwitcherPanelComponent,
  ],
  entryComponents: [
    ClassroomFullscreenComponent,
    StudentNameEditorComponent
  ],
  exports:  [
    ClassInfoComponent,
    ClassroomFullscreenComponent,
    CameraPresetMapComponent,
    LongPressDirective,
    RosterDrawerComponent,
    StudentNameEditorComponent,
    ControlDrawerComponent,
    VideoSwitcherPanelComponent
  ],
  providers: [
    CourseScheduleService
  ]
})
export class ClassroomModule {}