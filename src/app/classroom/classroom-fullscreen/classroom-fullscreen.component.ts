
import {interval as observableInterval, Observable, BehaviorSubject} from 'rxjs';

import {map} from 'rxjs/operators';
import {Component, OnInit, ViewChild, Input} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {MenuService} from "../../layout/menu.service";
import {DataService} from "../../data.service";
import {Endpoint, Control, Panel, Room} from "@devctrl/common";
import {CourseScheduleService, ICourseInfo, IDCCourseMeeting, nullCourseMeeting} from "../course-schedule.service";
import {ControlService} from "../../controls/control.service";
import {RoomConfig} from "../RoomConfig";


@Component({
  selector: 'devctrl-classroom-fullscreen',
  providers: [CourseScheduleService, ControlService],
  templateUrl: './classroom-fullscreen.component.html',
  styleUrls: [ './classroom-fullscreen.component.css']
})
export class ClassroomFullscreenComponent implements OnInit
{
  room : Room;
  panel : Panel;
  dataLoaded = false;

  timecodeBaseNum = 0;  // Last observed timecode
  timecodeBaseTime = 0; // Used to approximate current timecode
  timecode : Observable<string>;

  selectedCourseNumber = new BehaviorSubject<string>('');
  nextCourseMeeting : Observable<IDCCourseMeeting>;
  cMtg: IDCCourseMeeting = nullCourseMeeting;

  recordingAlert = false;
  recordingAlert2 = false;

  instructors: Observable<string>;
  courses = [];

  rc = new RoomConfig();



  constructor(private route : ActivatedRoute,
              public menu : MenuService,
              private ds : DataService,
              private css : CourseScheduleService,
              private controlService : ControlService) {}

  ngOnInit() {
    this.route.data.subscribe((data: {room: Room}) => {
      this.room = data.room;

      // Initialize any undefined config values
      for (let key in this.rc) {
        if (typeof this.room.config[key] === 'undefined') this.room.config[key] = this.rc[key];
      }

      this.rc = this.room.config;
    });

    this.nextCourseMeeting = this.css.nextCourseMeeting(this.rc.roomNumber, this.selectedCourseNumber);

    this.nextCourseMeeting.subscribe(cMtg => {
      this.cMtg  = cMtg;
      if (this.courseRecordAlertsActive(cMtg) && this.recordEndpointsEnabled()) {
        if (cMtg.msUntilStart < 60000 &&
          !this.recordStatus()) {
          this.recordingAlert = true;
        } else {
          this.recordingAlert = false;
        }

        if ((cMtg.msUntilEnd < 0 && this.recordStatus())
        || ( cMtg.msUntilEnd > 0 && cMtg.msUntilEnd < cMtg.msUntilStart && !this.recordStatus())) {
          this.recordingAlert2 = true;
        } else {
          this.recordingAlert2 = false;
        }
      } else {
        this.recordingAlert = false;
        this.recordingAlert2 = false;
      }
    });

    // The interval function ensures this is updated every second
    this.timecode = observableInterval(333).pipe(map((interval) => {
      let tcVal;
      let now = (new Date()).getTime();

      if (! this.rc.recordTsControlId) {
        tcVal = 0;
      }
      else {
        tcVal = this.controlService.getControl(this.rc.recordTsControlId).value;

        if (tcVal != this.timecodeBaseNum) {
          // Update
          this.timecodeBaseNum = tcVal;
          this.timecodeBaseTime = now;
        }

        if (tcVal > 0) {
          tcVal = this.timecodeBaseNum + Math.floor((now - this.timecodeBaseTime) / 1000);
        }
      }



      return this.formatTimecode(tcVal);
    }));

  }

  /**
   * Get the image source url for a camera.
   * @param {number} idx
   * @returns {any}
   */

  cameraSource(idx: number) {
    // If cameraViewUrl is defined for idx, use that
    if (this.rc.cameraViewUrls[idx]) return this.rc.cameraViewUrls[idx];

    if (! this.rc.cameraViewControlIds[idx]) {
      //console.log(`no camera view definition found for camera ${idx}`);
      return "";
    }

    let control = this.controlService.getControl(this.rc.cameraViewControlIds[idx]);

    if (! control.dataLoaded) {
      return "";
    }

    if (control.config["url"]) {
      return control.config["url"];
    }

    let path = control.config["path"];
    let proto = control.config["proto"];
    proto = proto ? proto : "https://";

    if (proto == "http" || proto == "https") {
      proto = proto + "://";
    }

    let host = control.config["host"];
    host = host ? host : control.endpoint.address;

    return proto + host + path;
  }

  courseList() : IDCCourseMeeting[] {
    return this.css.roomCourseSectionList(this.rc.roomNumber);
  }


  courseRecordAlertsActive(cm: IDCCourseMeeting): boolean {
    // Default to localstorage value defined on DataService.config
    if (typeof this.ds.config.courseAlerts === 'undefined') {
      this.ds.config.courseAlerts = {};
    }

    if (typeof this.ds.config.courseAlerts[cm.csid] !== 'undefined') {
      return this.ds.config.courseAlerts[cm.csid];
    }

    // Fall back to server default value
    return cm.recordAlert;
  }



  formatTimecode(tc: number) {
    if (tc == 0) { return '00:00:00'}

    let hourS = 3600;
    let remainingHours = Math.floor(tc / hourS);
    let minuteRemainder = tc - remainingHours * hourS;
    let minutes = Math.floor(minuteRemainder / 60);
    let secondRemainder = minuteRemainder - minutes * 60;

    let sec = ("00" + secondRemainder).slice(-2);

    let hours = ("00" + remainingHours).slice(-2);
    let min = ("00" + minutes).slice(-2);

    return `${hours}:${min}:${sec}`;
  }



  imgError($event) {
    console.log("error loading camera view");
  }

  isToolDrawerWide() {
    return this.rc.videoSwitcherLayout != 2;
  }

  recordButtonColor() {
    return this.recordStatus() ? "accent" : "";
  }


  recordButtonColorLayout2() {
    return this.recordStatus() ? "primary" : "warn";
  }

  recordButtonText() {
    return this.recordStatus() ? "Now Recording" : "Record";
  }

  recordEndpointsEnabled() {
    let allTrue = !! this.rc.recordControlIds.length;
    for (let id of this.rc.recordControlIds) {
      if (id && this.controlService.getControl(id).endpoint &&
        this.controlService.getControl(id).endpoint.epStatus.ok) {
        allTrue = allTrue && true;
      }
      else {
          allTrue = false;
      }
    }

    return allTrue;
  }


  recordStatus() {
    let allTrue = true;
    let allFalse = true;

    for (let id of this.rc.recordControlIds) {
      if (id) {
        allTrue = allTrue && this.controlService.getControl(id).value;
        allFalse = allFalse && (this.controlService.getControl(id).value === false);
      }
    }

    if (allTrue) return true;
    if (allFalse) return false;
    return null;
  }

  recordToggle() {
    let val = !this.recordStatus()

    for (let id of this.rc.recordControlIds) {
      this.controlService.setValue(val, id);
    }

    if (this.rc.recordTsControlId) {
      // Immediately update the timecode value when a recording is started/stopped
      let tcVal = val ? 1 : 0;
      this.controlService.getControl(this.rc.recordTsControlId).value = tcVal;
    }

  }

  setCourseRecordAlerts(val) {
    this.ds.config.courseAlerts[this.cMtg.csid] = val;
    console.log(`setting course alerts to ${val} for ${this.cMtg.csid}`);
    this.ds.updateConfig();
  }

  setSelectedCourse(val : string) {
    this.selectedCourseNumber.next(val);
  }

  sos() {
    this.ds.sos();
  }
}
