import {Component, Input, OnInit} from '@angular/core';
import { FormControl } from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {DataService} from "../data.service";
import { MatDialogRef } from '@angular/material';
import {ControlService} from "../controls/control.service";
import {Control} from "@devctrl/common";
import {CourseScheduleService} from "./course-schedule.service";
import {Observable, BehaviorSubject} from 'rxjs';
import {map, startWith} from 'rxjs/operators';


@Component({
    selector: 'devctrl-student-name-editor',
    providers: [CourseScheduleService],
    template: `
        <h2 mat-dialog-title color="primary">
            <span class="course-name">{{course}}</span> - 
            <span class="seat-number">Seat {{seat}}</span>
        </h2>
        <mat-dialog-content>
            <form class="student-form">
                <div class="form-container">
                    <mat-form-field>
                        <input  matInput name="name" 
                               placeholder="Name (Last, First)"
                               [ngModel]="name"
                               (ngModelChange)="updateOptions($event)"
                               [matAutocomplete]="auto">
                    </mat-form-field>
                    <mat-autocomplete autoActiveFirstOption #auto="matAutocomplete">
                        <mat-option *ngFor="let option of filteredOptions | async" [value]="option">
                            {{option}}
                        </mat-option>
                    </mat-autocomplete>
                </div>
            </form>
        </mat-dialog-content>
        <mat-dialog-actions>
            <div class="button-row">
                <button mat-button color="primary" (click)="updateName()">Update</button>
                <button mat-button (click)="close()">Cancel</button>
            </div>
        </mat-dialog-actions>
    
`,
    styles: [`

    `]
})
export class StudentNameEditorComponent implements OnInit
{
    _control : Control;
    course: string  = '';
    section: string = '';
    name: string = ''
    nameUpdates: BehaviorSubject<string> = new BehaviorSubject<string>('');
    seat: number;
    options = [];
    filteredOptions: Observable<string[]>;

    constructor(private ds : DataService,
                private cs : CourseScheduleService,
                public dialogRef: MatDialogRef<StudentNameEditorComponent>) {

    };

    ngOnInit() {
        this.filteredOptions = this.nameUpdates
            .pipe(
                startWith(''),
                map(value => this.filter(value))
            )
    }


    adminLogin() {
        this.ds.doAdminLogon();
    };

    close() {
        this.dialogRef.close();
    }

    get control() {
        return this._control;
    }

    set control(val: Control) {
        // This setter acts as an onInit function.  When this component is created this setter should be called
        // via the component instance ref.
        this._control = val;
        if (! this._control.config.names) this._control.config.names = {};
        if (! this._control.config.names[this.course]) this._control.config.names[this.course] = {};
        if (! this._control.config.names[this.course][this.section]) this._control.config.names[this.course][this.section] = {};
        if (! this._control.config.names[this.course][this.section]["options"]) {
            this._control.config.names[this.course][this.section]["options"] = [];
        }

        this.cs.courseRoster(this.course, this.section).subscribe( roster => {
            this.options = roster;
        });

        this.options = this._control.config.names[this.course][this.section]['options'];
    }

    private filter(name: string) {
        let val = name.toLowerCase();

        return this.options.filter(option => option.toLowerCase().includes(val));
    }

    updateOptions(name) {
        this.name = name;
        this.nameUpdates.next(name);

    }

    updateName() {
        this.ds.updateStudentNameConfig(this._control._id, this.course, this.section, this.seat, this.name);
        this.dialogRef.close();
    }
}