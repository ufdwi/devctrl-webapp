import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {CourseScheduleService, IDCCourseMeeting} from "../course-schedule.service";
import {Control} from '@devctrl/common';
import {Observable, combineLatest, BehaviorSubject} from 'rxjs';
import {distinctUntilChanged, map, flatMap} from 'rxjs/operators';
import {ControlService} from "../../controls/control.service";
import {DataService} from "../../data.service";

@Component({
  selector: 'devctrl-roster-drawer',
  providers: [CourseScheduleService, ControlService],
  templateUrl: './roster-drawer.component.html',
  styleUrls: ['./roster-drawer.component.css']
})
export class RosterDrawerComponent implements OnInit {

  @Input() presetControlIds: string[] = [];
  @Input() courseMeeting: Observable<IDCCourseMeeting>;
  @Output() close = new EventEmitter();
  public searchTerm = new BehaviorSubject('');
  public filteredStudents: Observable<string[]>;
  cm: IDCCourseMeeting;
  customName = "";

  constructor(private css: CourseScheduleService, private cs: ControlService, private ds: DataService) { }

  ngOnInit() {
    let currentMeeting = this.courseMeeting.pipe(
      distinctUntilChanged((m1, m2) => m1.csid === m2.csid)
    );

    currentMeeting.subscribe( cm => {
      this.cm = cm;
    });

    let currentRoster = currentMeeting.pipe(
      flatMap(cm => {
        return this.css.courseRoster(cm.courseCode, cm.section)
      })
    );

    let assignedNames = combineLatest(currentMeeting, this.ds.studentNameUpdates).pipe(
      map( ([cm, nameUpdate]) => {
        let names = [];
        for (let id of this.presetControlIds) {
          let ctrl = this.cs.getControl(id);
          if (ctrl.config
            && ctrl.config.names
            && ctrl.config.names[cm.courseCode]
            && ctrl.config.names[cm.courseCode][cm.section]) {
            names = names.concat(Object.values(ctrl.config.names[cm.courseCode][cm.section]));
          }
        }

        return names;
      })
    );

    this.filteredStudents = combineLatest(currentRoster, this.searchTerm, assignedNames).pipe(
      map(([roster, term, an]) => {
        let rosterUnassigned = roster.filter(name => !an.includes(name));
        console.log(`search term emitted: ${term}`);
        return rosterUnassigned.filter(name => name.toLowerCase().includes(term.toLowerCase()));
      })
    );
  }

  firstName(name) {
    let components = name.split(",");

    if (components[1]) { return components[1]}
    return "";
  }

  lastName(name) {
    return name.split(",")[0];
  }

  get customFirstname() {
    return this.firstName(this.customName);
  }

  set customFirstname(val) {
    if (val || this.lastName(this.customName)) {
      this.customName = this.lastName(this.customName) + "," + val;
    }
    else {
      this.customName = "";
    }
  }

  get customLastname() {
    return this.lastName(this.customName);
  }

  set customLastname(val) {
    if (val || this.firstName(this.customName)) {
      this.customName = `${val},${this.customFirstname}`;
    }
    else {
      this.customName = "";
    }
  }

  nameDropped($event) {
    if ($event.item.data.seat && $event.item.data.controlId) {
      // Unassign student from seat
      this.ds.updateStudentNameConfig($event.item.data.controlId,
        this.cm.courseCode,
        this.cm.section,
        $event.item.data.seat,
        '');
    }
  }


}
