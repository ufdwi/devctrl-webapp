import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RosterDrawerComponent } from './roster-drawer.component';

describe('RosterDrawerComponent', () => {
  let component: RosterDrawerComponent;
  let fixture: ComponentFixture<RosterDrawerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RosterDrawerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RosterDrawerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
