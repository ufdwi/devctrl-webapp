import {interval as observableInterval,  BehaviorSubject ,  Observable, combineLatest } from 'rxjs';
import {share, startWith, map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';


@Injectable()
export class CourseScheduleService {

  //private _courses : BehaviorSubject<ICourseInfo[]> = new BehaviorSubject([]);
  //courses = this._courses.asObservable();
  private courseMeetings : Observable<IDCCourseMeeting[]>;
  private cmList : IDCCourseMeeting[] = [];

  //This is the source URL.  TO get around CORS restrictions, we'll just store a local copy in courses.json
  // THis file will need to be updated every semester
  // Note: below doesn't work anymore - see https://one.uf.edu/soc/, search for courses, and look for URL used by AJAX request
  //private queryUrl = "https://one.ufl.edu/apix/soc/schedule/?category=RES&course-code=&course-title=&cred-srch=&credits=&day-f=&day-m=&day-r=&day-s=&day-t=&day-w=&days=false&dept=015851001&eep=&fitsSchedule=false&ge=&ge-b=&ge-c=&ge-d=&ge-h=&ge-m=&ge-n=&ge-p=&ge-s=&instructor=&last-row=0&level-max=--&level-min=--&no-open-seats=false&online-a=&online-c=&online-h=&online-p=&period-b=&period-e=&prog-level=+&term=20188&var-cred=true&writing="
  private term = "2201";
  private queryUrl;
  private lastCourseCode = '';
  public faketime = 1507212911538 + 24 * 60 * 60 * 1000 * 4; // a time
  private now = new Date();

  constructor(private http: HttpClient) {
    this.queryUrl = `/assets/courses.json`;
    this.courseMeetings = http.get(this.queryUrl).pipe(map(data => {
      let cms: IDCCourseMeeting[] = [];
      if (data[0]['COURSES']) {
        console.log("courses.json loaded");

        for (let course of <ICourseInfo[]>data[0]['COURSES']) {
          for (let section of course.sections) {
            let mDays = section.meetTimes.reduce((v, mt) => {
              if (v) v += ",";
              return v + mt.meetDays.join(",");
            }, '');
            for (let meetTime of section.meetTimes) {
              for (let day of meetTime.meetDays) {
                cms.push({
                  cmid: `${course.code}-${section.number}-${day}-${meetTime.meetPeriodBegin}`,
                  csid: `${course.code}-${section.number}`,
                  bldgCode: meetTime.meetBldgCode,
                  room: meetTime.meetRoom,
                  building: meetTime.meetBuilding,
                  day: day,
                  meetNo: meetTime.meetNo,
                  periodBegin: meetTime.meetPeriodBegin,
                  periodEnd: meetTime.meetPeriodEnd,
                  period: meetTime.meetPeriodEnd === meetTime.meetPeriodBegin ? meetTime.meetPeriodBegin :
                    `${meetTime.meetPeriodBegin}-${meetTime.meetPeriodEnd}`,
                  timeBegin: meetTime.meetTimeBegin,
                  timeEnd: meetTime.meetTimeEnd,
                  msUntilStart: 0,
                  msUntilEnd: 0,
                  section: section.number,
                  sectionMeetDays: mDays,
                  instructors: section.instructors,
                  courseCode: course.code,
                  courseName: course.name,
                  recordAlert: typeof course["recordAlert"] !== 'undefined' ? course["recordAlert"] : true  // recordAlert is optionally added manually
                });
              }
            }
          }
        }
      }
      else {
        console.error("invalid courses data recieved");
      }

      this.cmList = cms;
      return cms;
    }));

    observableInterval(1000).subscribe(() => {
      this.faketime += 75000;
      // Speedup at night
      let fnow = new Date(this.faketime);
      if (fnow.getHours() > 18 || fnow.getHours() < 9 || fnow.getDay() == 0 || fnow.getDay() == 6) {
        this.faketime += 750000;
      }
    })
  }



  courseRoster(course: string, section: string) : Observable<string[]> {
    return this.http.get<string[]>(`/fullscreen-classroom/rosters/course-students?course=${course}&section=${section}&term=${this.term}`).pipe(
      map( res => {
        if (res) {
          return res;
        }
        return [];
      }));
  }


  nextCourseMeeting(room: string, courseSectionNumber: BehaviorSubject<string> = new BehaviorSubject<string>('')): Observable<IDCCourseMeeting> {
    return combineLatest(observableInterval(1000), this.courseMeetings, courseSectionNumber).pipe(
      map(([interval, courseMeetings, csn], index) => {
        this.now = new Date(); //Date(this.faketime);
          let retCm = nullCourseMeeting;
          let nextCourseOffset = 7 * 24 * 60 * 60 * 1000 + 1;  // 7 days and a millisecond
          for ( let cm of courseMeetings) {
            if (cm.room === room) {
              cm.msUntilStart = this.timeUntilStarttime(cm);
              cm.msUntilEnd = this.timeUntilEndtime(cm);

              if (!csn || (csn === cm.csid)) {
                if (cm.msUntilEnd < nextCourseOffset) {
                  retCm = cm;
                  nextCourseOffset = cm.msUntilEnd;
                }
              }
            }
          }

          if (retCm.courseCode !== this.lastCourseCode) {
            console.log("CourseScheduleService: next course is " + retCm.courseCode);
            this.lastCourseCode = retCm.courseCode;
          }

          return retCm;
        }
      ),
      share());
  }



  roomCourseSectionList(room: string) : IDCCourseMeeting[] {
    let csids = {};
    return this.cmList.filter(cm => {
      if (cm.room === room) {
        if (csids[cm.csid]) {
          return false;
        }
        else {
          csids[cm.csid] = true;
          return true;
        }
      }

      return false;
    });
  }


  timeUntilEndtime(cm:  IDCCourseMeeting) {
    return this.timeUntilMeetTime(cm.timeEnd, cm.day);
  }

  timeUntilStarttime(cm:  IDCCourseMeeting) {
    let startDiff = this.timeUntilMeetTime(cm.timeBegin, cm.day);
    if (startDiff < 0) {
      startDiff += 7 * 24 * 60 * 60 * 1000;
    }

    return startDiff;
  }


  timeUntilMeetTime(time: string, day: string) {
    let now = this.now;

    let amPm = time.slice(-2);
    let hm  = time.slice(0, -3);
    let hmSplit = hm.split(":");

    let endHour = +hmSplit[0];
    if (endHour == 12) {
      // 12 AM is 00:00, 12 PM is 12:00
      endHour = 0;
    }

    if (amPm == "PM") {
      endHour += 12;
    }

    let endMinute = +hmSplit[1];

    let endTimeToday = new Date(now.getFullYear(), now.getMonth(), now.getDate(), endHour, endMinute);
    let endTimeOffset = endTimeToday.getTime() - now.getTime();

    let dayDiff = 8;

    let diff = this.dowDiff(day, now.getDay());
    // Allow negative endtime offsets up to 5 minutes
    if (endTimeOffset < -1000 * 60 * 5 && diff == 0) {
      diff = 7;
    }

    if (diff < dayDiff) {
      dayDiff = diff;
    }


    let dayMs = 1000 * 60 * 60 * 24;

    return dayDiff * dayMs + endTimeOffset;
  }

  /**
   * Calculate the number of days from dayInt (0-6) until the next dayStr (M,T,W,R,F)
   */
  dowDiff(dayStr: string, dayInt: number) {
    let dow = {
      "M": 1,
      "T": 2,
      "W": 3,
      "R": 4,
      "F": 5
    };

    if (! dow[dayStr]) {
      console.error("invalid day of week identifier: " + dayStr);
      return 8;
    }

    let diff = dow[dayStr] - dayInt;
    if (diff < 0) {
      diff += 7;
    }

    return diff;
  }
}

export interface ISectionInfo {
  EEP?: string;
  LMS?: string;
  courseFee?: string;
  credits?: string;
  dNote?: string;
  deptCode: string;
  deptName: string;
  display: string;  // The section number
  finalExam?: string;
  genEd?: any[];
  grWriting?: string;
  instructors: IInstructorInfo[];
  meetTimes: IMeetTime[];
  nextMeetTimeIdx?: number;
  note?: string;
  number: string; // Also the section number
  rotateTitle?: string;
  sectWeb?: string;
}


export interface ICourseInfo {
  cNote: string;
  code: string;
  name: string;
  sections: ISectionInfo[]
  termInd: string;
  nextSectionIdx?: number;
  nextSectionDisplay?: string;
  nextPeriod?: string;
}

export interface IInstructorInfo {
  name: string;
}

export interface IMeetTime {
  meetBldgCode: string;
  meetRoom: string;
  meetBuilding: string;
  meetDays: string[];
  meetNo: number;
  meetPeriodBegin: string;
  meetPeriodEnd: string;
  meetTimeBegin: string;
  meetTimeEnd: string;
  msUntilStart?: number;
  msUntilEnd?: number;
}

/**
 *  Information from UF provided class listings will be reformatted into these objects
 */

export interface IDCCourseMeeting {
  cmid: string;
  csid: string; // ID with course and section codes
  bldgCode: string;
  room: string;
  building: string;
  day: string;
  meetNo: number;
  periodBegin: string;
  periodEnd: string;
  period: string;
  timeBegin: string;
  timeEnd: string;
  msUntilStart: number;
  msUntilEnd: number;
  section: string;
  sectionMeetDays: string;
  instructors: IInstructorInfo[];
  courseCode: string;
  courseName: string;
  recordAlert: boolean;
}

export let nullCourseMeeting = {
    cmid: '',
  csid: '',
  bldgCode: '',
  room: '',
  building: '',
  day: '',
  meetNo: 0,
  periodBegin: '',
  periodEnd: '',
  period: '',
  timeBegin: "12:00 AM",
  timeEnd: "12:00 AM",
  msUntilEnd: 0,
  msUntilStart: 0,
  section: '',
  sectionMeetDays: '',
  instructors: [],
  courseCode: '',
  courseName: '',
  recordAlert: true
};
