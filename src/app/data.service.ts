import {UserSession} from "@devctrl/common";
import * as io from "socket.io-client";
import {
    IDCDataRequest, IDCEndpointStatusUpdate, DCSerializable, DCSerializableData,
    IDCTableDefinition, IDCDataExchange, IDCStudentNameUpdate,
    IDCDataAdd
} from "@devctrl/common";
import {ControlUpdateData} from "@devctrl/common";
import {DCDataModel, IDCSchema, IndexedDataSet} from "@devctrl/common";
import {Control} from "@devctrl/common";
import {Endpoint} from "@devctrl/common";
import { Injectable, Inject } from "@angular/core";
//import { Headers, Http } from '@angular/http';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar, MatDialog } from '@angular/material';

import {AlertDialog} from "./alert-dialog.component";
import {EndpointType} from "@devctrl/common";
import {OptionSet} from "@devctrl/common";
import {Panel} from "@devctrl/common";
import {PanelControl} from "@devctrl/common";
import {Room} from "@devctrl/common";
import {ActionTrigger} from "@devctrl/common";
import {ActionLog} from "@devctrl/common";
import {UserInfo} from "@devctrl/common";
import { Observable, ReplaySubject, throwError, BehaviorSubject } from 'rxjs';
import { catchError, retry, take } from 'rxjs/operators';


@Injectable()
export class DataService {
    appVersion = "5.6.3";
    messengerVersion = "";
    socket: SocketIOClient.Socket;
    //private http : Http;
    userSession : UserSession;
    private initialized = false;
    private dataModel : DCDataModel;
    private controlsByCtid = {};
    pendingUpdates : Promise<void>[] = [];
    userInfoSubject = new ReplaySubject(1);
    userInfoRequested = false;
    adminLogonTimeout;
    studentNameUpdates = new BehaviorSubject('');
    tablePromises = {};
    tableSubjects = {};
    config = {
        editEnabled: true,
        lastLogonAttempt: 0,
        menu : {
            sidenavOpen : false
        },
        courseAlerts: {
        }
    };
    logs : ActionLog[] = [];


    constructor(
                private http : HttpClient,
                private snackBar : MatSnackBar,
                private mdDialog : MatDialog) {
        /**
        for (let table in this.schema) {
            let tschema = this.schema[table];
            if (tschema.foreign_keys) {
                tschema['referenced'] = {};
                for (let keyName in tschema.foreign_keys) {
                    tschema.referenced[tschema.foreign_keys[keyName]] = keyName;
                }
            }
        }
         **/

        this.userSession = {
            _id: '',
            username: null,
            login_expires: 0,
            auth: false,
            admin_auth: false,
            admin_auth_expires: 0
        };

        this.dataModel = new DCDataModel();

        if (typeof(window.localStorage) !== 'undefined') {
            let localConfig = window.localStorage['config'];
            if (typeof localConfig == 'string') {
                this.config = JSON.parse(localConfig);
            }
            else {
                window.localStorage['config'] = JSON.stringify(this.config);
            }
        }
        else {
            console.log("window.localStorage not available");
        }
    }

    addRow(row : DCSerializable, callback) {
        let req : IDCDataAdd = {
            _id: this.guid(),
            userInfo_id: this.userSession.userInfo_id,
            add : {}
        };
        req.add[row.table] = [row.getDataObject()];

        this.socket.emit('add-data', req, (response : IDCDataExchange) => {
            if (response.error) {
                this.errorToast(response.error);
                return;
            }

            let newId = Object.keys(response.add[row.table])[0];
            console.log("new record " + newId + " added to " + row.table);

            this.loadData(response);

            let record = this.dataModel.tables[row.table][newId];

            if (typeof callback == 'function') {
                callback(record);
            }
        });
    }


    deleteRow(row : DCSerializable) {


        // Check for foreign key constraints
        let referencedTable = "";
        for(let ref in row.referenced) {
            if (Object.keys(row.referenced[ref]).length > 0) {
                referencedTable = ref;
            }
        }

        if (referencedTable) {
            let msg = "Cannot delete " + row.table + " record due to foreign key constraint on " + referencedTable;

            this.errorToast({error: msg});
            return;
        }

        this.deleteRowObservable(row).subscribe(
            (data: IDCDataExchange) => this.loadData(data),
            error => this.handleHttpError(error)
        );

    }

    deleteRowObservable(row : DCSerializable) : Observable<{}> {
        let resource = "api/data/" + row.table + "/" + row._id;

        let httpOptions = {
            headers: new HttpHeaders({})
        };

        return this.http.delete(resource, httpOptions);
    }

    dialogClose() {
        this.mdDialog.closeAll();
    }

    doAdminLogon(allowExpiration : boolean = false) {
        // First, check current login status
        if (this.userSession.login_expires > Date.now()) {

            this.doAdminLogonObservable().subscribe(
                //Success
                response => {
                    Object.assign(this.userSession, response.session);

                    // Refresh credentials before they expire
                    let retryDelay = (this.userSession.admin_auth_expires - Date.now()) - 2000;
                    //let retryDelay = 60000;
                    let retryMilli = Date.now() + retryDelay;
                    let retryTime = new Date(retryMilli);
                    let retryTimeStr = retryTime.toTimeString().substr(0, 17);


                    if (retryMilli > this.userSession.login_expires) {
                        console.log("not scheduling admin refresh, user session will be expired");
                        return;
                    }

                    if (retryDelay > 0) {
                        this.adminLogonTimeout = setTimeout(() => {
                                this.doAdminLogon(true);
                            },
                            retryDelay
                        );

                        console.log(`admin_auth successful, admin refresh scheduled for ${retryTimeStr}`);
                    }
                    else {
                        console.log("admin_auth successful, negative retry delay, what the hell does that mean?");
                    }
                },
                //Failure
                response => {
                    if (response.status == 401) {
                        this.errorToast("You are not authorize to access admin functions");

                        //TODO: admin_auth_requested should be set on local storage
                        this.userSession.admin_auth_requested = false;
                    }
                    else if (response.status == -1 || response.status == 0) {
                        if (! allowExpiration) {
                            console.log("XHR admin auth failed, attempting logon");
                            this.doLogon(true, true);
                        }
                        else {
                            console.log("XHR admin auth failed, dropping admin privileges")
                        }
                    }
                    else {
                        this.errorToast("Admin auth error: " + response.status);
                    }
                }
            )
        }
        else {
            let expireStr = (new Date(this.userSession.login_expires)).toTimeString().substr(0, 17);
            console.log(`user session expired at ${expireStr}, referring to doLogon`);
            if (this.config.lastLogonAttempt) {
                let lastLogonStr = (new Date(this.config.lastLogonAttempt)).toTimeString().substr(0, 17);
                console.log(`last logon attempt was at ${lastLogonStr}`);
            }

            this.doLogon(true);
        }
    }

    doAdminLogonObservable() {
        let url = "/auth/admin/get_auth";
        return this.http.get<{ session: UserSession}>(url);
    }

    doLogon(admin_auth_check: boolean = false, force_login: boolean = false) {
        if (force_login || this.userSession.login_expires < Date.now()) {
            // Circuit breaker for login loops
            if (this.config.lastLogonAttempt) {
                let timeSinceLogon = Date.now() - this.config.lastLogonAttempt;
                if (timeSinceLogon < 10000) {
                    this.errorToast("Login loop detected.  Not processing logon request");
                    return;
                }
            }

            this.config.lastLogonAttempt = Date.now();
            this.updateConfig();

            let location = window.location.pathname;
            let newLocation = "/auth/do_logon?";

            if (admin_auth_check) {
                newLocation = newLocation + "admin_auth_requested=1&";
            }

            console.log("doLogon: admin_auth_requested = " + admin_auth_check);

            newLocation = newLocation + "location=" + location;
            window.location.href = newLocation;
        }
    }


    errorToast(data) {
        let errorText = "An unknown error has occured"
        if (data.error) {
            errorText = data.error;
        }
        else if (typeof data == 'string') {
            errorText = data;
        }

        console.log(errorText);

        //$mdToast.show($mdToast.simple().content(errorText));

        this.snackBar.open(errorText, "OK");
    }

    generateEndpointConfig($event, endpointId : string) {
        let endpoint = this.getRowRef(Endpoint.tableStr, endpointId);
        let endpointName = endpoint.name.toLowerCase();
        endpointName = endpointName.replace(/ /g, '-');

        let url = `/auth/admin/create_endpoint_session?${endpointName}`;
        this.generateEndpointConfigObservable(url).subscribe(
            //Success
            response => {
                let alert = `{
    "endpointId": "${endpointId}",
    "authId": "${response.session._id}"
}               
                `;

                let ref = this.mdDialog.open(AlertDialog);
                ref.componentInstance.title = `/etc/devctrl/${endpointName}.json`;
                ref.componentInstance.content = alert;
                ref.componentInstance.ok = "Got It!";
            },
            error => this.handleHttpError(error)
        );
    }

    private generateEndpointConfigObservable(url : string) {
        return this.http.get<{ session: UserSession }>(url);
    }


    /*
    *  Retrieve a control by it's ctid value
     */
    getControlByCtid(ctid: string) {
        // Keep an indexed object of controls that have been looked up.  CTID should be immutable
        if (this.controlsByCtid[ctid]) {
            return this.controlsByCtid[ctid];
        }

        // Search for it, if it hasn't been found before
        let controls = <IndexedDataSet<Control>>this.getTable(Control.tableStr);
        for (let id of Object.keys(controls)) {
            if (controls[id].ctid == ctid) {
                this.controlsByCtid[ctid] = controls[id];
                return controls[id];
            }
        }
    }

    getNewRowRef(tableName: string, newData = {}) {
        let ctor = this.dataModel.types[tableName];

        let newRow = new ctor("0");
        Object.assign(newRow, newData);

        return newRow;
    }

    /*
     * data row properties:
     *   id - primary key value
     *   fields - other columns as properties
     *   foreign - foreign key records
     *   loaded - has field data been loaded?
     *   referenced - other rows that reference this row
     */

    getRowRef(tableName: string, key: string) : DCSerializable {
        if (! tableName) {
            throw new Error("error looking up record for undefined table");
        }

        if (! key || key === null) {
            throw new Error(`error looking up ${tableName} record for undefined key`);
        }

        return this.dataModel.getItem(key, tableName);
    }


    get schema() : IDCSchema {
        return this.dataModel.schema;
    }


    getTable(table: string) : IndexedDataSet<DCSerializable> {
        if (! this.tablePromises[table]) {
            this.tablePromises[table] = this.getTablePromise(table);
        }

        return this.dataModel.tables[table];
    }


    /**
     * Get a promise object representing a request for table data
     * @param table
     * @returns {*}
     */
    getTablePromise(table: string) {
        return this.getTableObservable(table).pipe(take(1)).toPromise();
    }

    getTableObservable(table: string) : Observable<IndexedDataSet<DCSerializable>> {
        if (! this.initialized) {
            this.init();
        }

        if (! table) {
            console.error("error: attempt to fetch undefined table!");
            return throwError("error: attempt to fetch undefined table!");
        }


        if (this.tableSubjects[table]) {
            return this.tableSubjects[table].asObservable();
        }

        let req : IDCDataRequest = {
            _id: this.guid(),
            table: table,
            params: {},
        };

        this.tableSubjects[table] =  new ReplaySubject<IndexedDataSet<DCSerializable>>(1);
        this.socket.emit('get-data', req, (data) => {
            console.log(`getTableObservable: ${table} data received`);

            if (data.error) {
                return throwError(data.error);
            }

            this.loadData(data);
            this.tableSubjects[table].next(this.dataModel.tables[table]);
        });


        return this.tableSubjects[table].asObservable();
    }


    getUserInfo() {
        if (! this.userInfoRequested) {
            let userInfoUrl = "/auth/user_session";
            this.http.get<{session: UserSession}>(userInfoUrl).subscribe(
                    //Success
                    response => {
                        Object.assign(this.userSession, response.session);
                        this.userInfoSubject.next(this.userSession);

                        // If we are logged in, check for admin auth
                        if (this.userSession.admin_auth_requested) {
                            console.log("getUserInfo doAdminLogon");
                            this.doAdminLogon();
                        }
                        else if (!this.userSession.auth) {
                            // User is not authorized application and should be directed to log on
                            console.log("getUserInfo doLogon");
                            this.doLogon();
                        }
                        else if (this.isAdminAuthorized()) {
                            // Setup periodic checks to renew admin auth
                            this.doAdminLogon(true);
                        }
                    },
                    //Failure
                    response => {
                        console.log("get user session failed with response code:" + response.status);
                        //TODO: we should prevent the user from using the application at this point
                    }
                );
        }

        return this.userInfoSubject.asObservable();
    }

    guid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            let r = Math.random()*16|0;
            let v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    }

    hideToast() {
        //this.$mdToast.hide();
    }

    private handleHttpError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }

        this.errorToast(error);
    }

    init() {
        if (this.initialized) {
            return;
        }

        this.socket = io.connect(window.location.protocol + "//" + window.location.host,
            {
                //path: "/socket-dev.io",
                path: "/socket.io",
                transports: ["websocket"]
            }
        );

        this.socket.on('control-data', (data: IDCDataExchange) => {

            if (data.userInfo_id == this.userSession.userInfo_id) {
                console.log("discarding locally originated data update:" + data.userInfo_id);
            }
            else {
                this.loadData(<IDCDataExchange>data);
                //console.log("socket control data received");
            }
        });

        this.socket.on('control-updates', data => {
            this.loadControlUpdates(data);
        });

        this.socket.on('log-data', data => {
            //this.dataModel.applog.push(data);
        });

        this.socket.on('reconnect', () => {
            // Refresh endpoints, as they may have been disconnected from the messenger
            console.log("reconnect, fetching data");
            this.getTableObservable(Endpoint.tableStr);
        });

        this.socket.on('messenger-data', data => {
            this.messengerVersion = data.version;
            console.log(`connected to messenger version ${this.messengerVersion}`);
        });

        this.socket.on('connect', () => {
            // Just load everything
            this.getUserInfo().subscribe(userInfo => {
                this.getTableObservable(Control.tableStr).subscribe();
                this.getTableObservable(Endpoint.tableStr).subscribe();
                this.getTableObservable(EndpointType.tableStr).subscribe();
                this.getTableObservable(OptionSet.tableStr).subscribe();
                this.getTableObservable(Panel.tableStr).subscribe();
                this.getTableObservable(PanelControl.tableStr).subscribe();
                this.getTableObservable(Room.tableStr).subscribe();
                this.getTableObservable(ActionTrigger.tableStr).subscribe();
                this.getTableObservable(UserInfo.tableStr).subscribe();
            });
        });

        this.getUserInfo().subscribe();
        console.log("dataService2 initialized");
        this.initialized = true;
    }


    isAdminAuthorized() {
        return this.userSession.admin_auth && (this.userSession.admin_auth_expires > Date.now());
    }


    /*
     * loadControlUpdates is process separately from loadData, as loadControlUpdates is
     * much more limited in the scope of changes to the data model
     */
    loadControlUpdates(updates: ControlUpdateData[]) {
        for (let update of updates) {
            if (update.status == "observed" || update.status == "executed") {
                let control = <Control>this.getRowRef("controls", update.control_id);
                control.value = update.value;
            }
        }
    }

    loadData(data : IDCDataExchange) {
        try {
            this.dataModel.loadData(data);
        }
        catch (e) {
            console.error("loadData error: " + e.message);
        }
    }

    logAction(message: string, typeFlags: string[] = [], referenceList: string[] = []) {
        let id = this.guid();
        /**
        let action = new ActionLog(id,
            {
                _id: id,
                name : message,
                timestamp: Date.now(),
                typeFlags: typeFlags,
                referenceList: referenceList,
                user_session_id: this.userSession._id
            }
        );

        this.logs.unshift(action); // Use unshift to make it easier to view recent logs first
        **/

        console.log(`action logged: ${message}`);
    }

    revokeAdminAuth() {
        //TODO: this doesn't do anything unless the Apache auth session credentials
        // are also revoke
        this.userSession.admin_auth_expires = Date.now();

        if (this.adminLogonTimeout) {
            clearTimeout(this.adminLogonTimeout);
        }

        let url = "/auth/revoke_admin";
        this.revokeAdminAuthObservable().subscribe(
            //Success
            response => {
                Object.assign(this.userSession, response.session);
                console.log("revoke admin auth successful");
            },
            //Failure
            response => {
                this.errorToast("Revoke Admin auth error: " + response.status);

            }
        )
    }

    revokeAdminAuthObservable() {
        let url = "/auth/revoke_admin";
        return this.http.get<{session: UserSession}>(url);
    }


    sortedArray(table : string, sortProp : string = 'name') : DCSerializable[] {
        return this.dataModel.sortedArray(table, sortProp);
    }

    // Send a help request
    sos() {
        let reqData = {};
        this.socket.emit('sos', reqData, data => {

        });

        this.snackBar.open("A help request has been sent", "OK");
    }


    publishStatusUpdate(message: string) {
        this.socket.emit('status-update', { message: message});
    }

    updateConfig() {
        if (typeof(window.localStorage) !== 'undefined') {
            window.localStorage['config'] = JSON.stringify(this.config);
        }
    }

    updateControlValue(control : Control) {
        if (this.pendingUpdates[control._id]) {
            clearTimeout(this.pendingUpdates[control._id]);
        }

        this.pendingUpdates[control._id] = setTimeout(() => {
            let cuid = this.guid();
            let updates : ControlUpdateData[] = [
                {
                    _id: cuid,
                    name: control.name + " update",
                    control_id: control._id,
                    value: control.value,
                    type: "user",
                    status: "requested",
                    source: this.userSession._id
                }
            ];

            console.log(`control-update issued. ${control.name} : ${control.value}`);
            this.socket.emit('control-updates', updates);
            return false;
        }, 100, false);
    }

    /*
    *  Set the enabled value for an endpoint.  Do this using a dedicated API endpoint with separate authentication
    *  requirements
    *
     */
    updateEndpointEnabled(deviceId: string, enabled: boolean) {
        let reqData : IDCEndpointStatusUpdate = {
            _id: this.guid(),
            userInfo_id: this.userSession.userInfo_id,
            endpoint_id: deviceId,
            status : {
                enabled: enabled
            }
        };

        this.socket.emit('update-endpoint-enabled', reqData, data => {
            if (data.error) {
                console.error("update endpoint enabled error: " + data.error);
                this.errorToast(data);
                return;
            }

            this.loadData(data);
            let ep = this.getRowRef(Endpoint.tableStr, deviceId);
            let enStr = enabled ? 'enabled' : 'disabled';
            console.log(`endpoint status update: ${ep.name} ${enStr}`);
        })
    }



    updateRow(row : DCSerializable) {
        let reqData : IDCDataExchange = {
            _id: this.guid(),
            userInfo_id: this.userSession.userInfo_id,
            add : {}
        };
        reqData.add[row.table] = {};
        reqData.add[row.table][row._id] = row.getDataObject();

        this.socket.emit('update-data', reqData, data => {
            if (data.error) {
                console.error("update data error: " + data.error);
                this.errorToast(data);
                return;
            }
            console.log("data updated");
            console.dir(data);
            //this.loadData(data);  // this is redundant
        });
    }

    updateStudentNameConfig(controlId: string, course: string, section: string, seat: number, name: string) {
        let studentNameUpdate : IDCStudentNameUpdate = {
            _id: this.guid(),
            control_id: controlId,
            course: course,
            section: section,
            seat: seat,
            name: name
        };

        this.socket.emit('update-student-name-config', studentNameUpdate, data => {
            if (data.error) {
                console.error("student name config updated failed: " + data.error);
                this.errorToast(data);
                return;
            }

            console.log("update student name config completed");
            console.dir(studentNameUpdate);
            this.loadData(<IDCDataExchange>data);
            this.studentNameUpdates.next('');
        });
    }

}
