import { NgModule }  from '@angular/core';
import {AdminOnlyDirective} from "./admin-only.directive";

@NgModule({
    declarations: [
        AdminOnlyDirective,
    ],
    exports: [
        AdminOnlyDirective,
    ]
})
export class DevctrlCommonModule {}