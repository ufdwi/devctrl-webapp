import {Room} from "@devctrl/common";
import {DataService} from "../data.service";
import { Component, OnInit, Input } from '@angular/core';
import {MenuService} from "../layout/menu.service";
import {LayoutService} from "../layout/layout.service";

@Component({
    selector: 'devctrl-rooms',
    template: `
<div  id="devctrl-content-canvas">
    <div class="devctrl-card">
        <mat-grid-list [cols]="cols()"
                      rowHeight="1:1"
                      gutterSize="8px">
            <mat-grid-tile *ngFor="let room of list; let i = index;"     
                        (click)="menu.go(['rooms', room.name])">
                <div class="devctrl-img">
                    <img [src]="imageUrl(room)" />
                    <a mat-raised-button *ngIf="room.config.presetMapControlIds" 
                       color="primary"
                       class="classroom-link"
                       [routerLink]="['/classroom', room.name]">
                        INSTRUCTOR INTERFACE
                    </a>
                </div>
            </mat-grid-tile>
        </mat-grid-list>
    </div>
</div>
`,
    //language=CSS
    styles: [`   
        .classroom-link {
            position: absolute;
            top: 80%;
            right: 10%;
        }
        .devctrl-card {
            flex: 1 1;
        }
        .devctrl-img {
            flex: 1 1;
        }
        .devctrl-img img {
            width: 100%;
            min-width: 200px;
        }
`]
})
export class RoomsComponent implements OnInit {
    list : Room[];
    static colorClasses = [
        'yellow', 'red', 'purple', 'green', 'pink', 'darkBlue'
    ];

    constructor(private dataService: DataService,
                private ls: LayoutService,
                private menu : MenuService) {
        console.log("rooms component created");
        this.list = this.dataService.sortedArray(Room.tableStr, "name") as Room[];
    }

    ngOnInit() {
        console.log("rooms component initialized");
        this.menu.currentTopLevel = MenuService.TOPLEVEL_ROOMS;
        this.menu.pageTitle = "Rooms";
        this.menu.parentName = "";
    }

    cols() {
        if (this.ls.desktopWide) {
            return 4;
        }

        return 2;
    }

    imageUrl(room) {
        return `/assets/images/${room.name}.png`;
    }
}