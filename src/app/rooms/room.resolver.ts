import { Injectable }             from '@angular/core';
import { Router, Resolve, RouterStateSnapshot,
    ActivatedRouteSnapshot } from '@angular/router';
import { DataService } from '../data.service';
import {Room} from "@devctrl/common";
import {Observable} from 'rxjs';
import { map, take } from 'rxjs/operators';
import {IndexedDataSet} from "../../../../common/DCDataModel";

@Injectable()
export class RoomResolver implements Resolve<Room> {
    constructor(private ds: DataService, private router: Router) {}
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Room> {
        let name = route.params['name'];
        console.log("room resolver invoked");

        let roomsObservable = <Observable<IndexedDataSet<Room>>>this.ds.getTableObservable(Room.tableStr);

        return roomsObservable.pipe(
            take(1),
            map(rooms => {
                if (rooms[name]) {
                    // We were given an id, reroute to name
                    console.log(`rerouting from room ${name} to ${rooms[name].name}`);
                    this.router.navigate(['/rooms', rooms[name].name]);
                    return rooms[name];
                }


                for (let id in rooms) {

                    if (rooms[id].name.toLowerCase() == name.toLowerCase()) {
                        console.log(`RoomResolver resolved ${name}`)
                        return rooms[id];
                    }
                }


                console.log(`RoomResolver: Room ${name} not found`);
                this.router.navigate(['/rooms']);
                return null;
            })
        );
    }
}