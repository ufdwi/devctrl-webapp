import { Component, OnInit } from '@angular/core';
import {RecordComponent} from "../../data-editor/record.component";
import {DataService} from "../../data.service";
import { MatDialogRef } from '@angular/material';
import { Room, Panel, IndexedDataSet } from '@devctrl/common';
import {RecordEditorService} from "../../data-editor/record-editor.service";


@Component({
  selector: 'app-panel-group-dialog',
  templateUrl: './panel-group-dialog.component.html',
  styleUrls: ['./panel-group-dialog.component.css']
})
export class PanelGroupDialogComponent implements OnInit {
  group : string = "";
  room : Room;

  constructor(private dataService: DataService,
              public dialogRef: MatDialogRef<RecordComponent>,
              private recordService : RecordEditorService) { }

  ngOnInit() {
  }

    addPanel($event) {
        this.recordService.editRecord($event, '0', 'panels',
            {
                'room' : this.room,
                'grouping' : this.group
            }
        );
    }

  close() {
    this.dialogRef.close();
  }

  set roomId(id: string) {
    this.room = <Room>this.dataService.getRowRef(Room.tableStr, id);
  }

  get roomId() {
    if (this.room) {
      return this.room._id;
    }

    return "";
  }

  getPanels() {
    if (! this.room) return [];

    let panels = Object.values(<IndexedDataSet<Panel>>this.room.referenced[Panel.tableStr]).filter( panel => {
      return panel.grouping == this.group;
    });

    return panels;
  }

}
