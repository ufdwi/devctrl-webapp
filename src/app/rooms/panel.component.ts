import {Component, Input} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {Panel} from "@devctrl/common";
import {DataService} from "../data.service";
import {RecordEditorService} from "../data-editor/record-editor.service";
import {PanelControl} from "@devctrl/common";
import {IndexedDataSet} from "@devctrl/common";
import {Control} from "@devctrl/common";
import {DCSerializable} from "@devctrl/common";

@Component({
    selector: 'devctrl-panel',
    template: `
<h3 mat-subheader>
    {{panelObj.name}}
    <button mat-button class="md-primary"  
            *ngIf="isSwitchGroup()"
            (click)="setAllSwitches(true)">
        All On
    </button>
    <button mat-button class="md-warn" 
            *ngIf="isSwitchGroup()"
            (click)="setAllSwitches(false)">
        All Off
    </button>
    <button mat-button *devctrlAdminOnly
            (click)="addControl($event)"
            class="md-primary">
        Add Control
    </button>
    <button mat-button *devctrlAdminOnly
            (click)="editPanel($event)"
            class="md-primary mat-hue-1">
        Edit Panel
    </button>
</h3>

<section [ngSwitch]="panelObj.type">
    <mat-list-item *ngSwitchCase="'horizontal'" class="devctrl-ctrl-list-item">
        <div class="hpanel">
            <devctrl-ctrl *ngFor="let pcontrol of pcList(); trackBy:trackById"
                [panelControl]="pcontrol"></devctrl-ctrl>
        </div>    
    </mat-list-item>
    <ng-template ngSwitchDefault>
        <mat-list-item *ngFor="let pcontrol of pcList(); trackBy:trackById"
                    class="devctrl-ctrl-list-item">
            <devctrl-ctrl [panelControl]="pcontrol"></devctrl-ctrl>
        </mat-list-item>
    </ng-template>
</section>
<mat-divider></mat-divider>
`,
    styles: [`
.hpanel {
    display: flex;
    flex-direction: row;
    width: 100%;
}

.hpanel devctrl-ctrl {
    flex: 1 1;
}
`]

})
export class PanelComponent
{
    @Input()panelObj : Panel;

    constructor(private dataService : DataService,
                private recordService : RecordEditorService) {}

    addControl($event) {
        this.recordService.editRecord(
            $event, "0", PanelControl.tableStr,
            { panel: this.panelObj}
        );
    };

    editPanel($event) {
        this.recordService.editRecord($event, this.panelObj._id, this.panelObj.table);
    };

    isSwitchGroup() {
        return this.panelObj.type == 'switch-group';
    }

    pcList() {
        let pcArray = Object.keys(this.panelObj.referenced[PanelControl.tableStr])
            .map(id => {
                return this.panelObj.referenced[PanelControl.tableStr][id] as PanelControl;
            });

        pcArray.sort((a, b) => {
            if (typeof b.idx == 'undefined') {
                return -1;
            }
            if (typeof a.idx == 'undefined') {
                return 1;
            }

            return a.idx - b.idx;
        })

        return pcArray;
    }

    setAllSwitches(val) {
        let pcList = <IndexedDataSet<PanelControl>>this.panelObj.referenced[PanelControl.tableStr];
        for (let pcId in pcList) {
            let control = pcList[pcId].control;

            if (control.usertype == Control.USERTYPE_SWITCH) {
                control.value = val;
                this.dataService.updateControlValue(control);
            }
        }
    };

    trackById = DCSerializable.trackById;
}