import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import {DataService} from "../data.service";
import {PanelGroupDialogComponent} from "./panel-group-dialog/panel-group-dialog.component";
import {ImageDialogComponent} from "../image-dialog.component";

@Injectable()
export class PanelDialogService {

  constructor(private dataService: DataService,
              private matDialog : MatDialog) { }

  openPanelGroup($event, group : string, roomId : string) {
    let panelRef = this.matDialog.open(PanelGroupDialogComponent);
    panelRef.componentInstance.group = group;
    panelRef.componentInstance.roomId = roomId;
  }

  openImageDialog($event, title: string, src: string) {
    let imageRef = this.matDialog.open(ImageDialogComponent);
    imageRef.componentInstance.title = title;
    imageRef.componentInstance.src = src;
  }
}
