import { Component, OnInit} from '@angular/core';
import {DataService} from "../data.service";
import {MenuService} from "./menu.service";
import {  ActivatedRoute, Router } from '@angular/router';
import {MediaService} from "./media.service";
import {LayoutService} from "./layout.service";


@Component({
    selector: 'devctrl-app',
    template: `
<div (window:resize)="ls.resized($event)">
    <devctrl-toolbar></devctrl-toolbar>    
    <mat-sidenav-container class="dc-sidenav-container" [class.fullscreen]="menuService.fullscreen">
        <mat-sidenav class="dc-sidenav"
                    [opened]="menu.isSidenavOpen()"
                    [mode]="sidenavMode()">
            <devctrl-menu></devctrl-menu>
        </mat-sidenav>
        <router-outlet></router-outlet>
    </mat-sidenav-container>
</div>
`,
    styles: [`
        .dc-sidenav {
            width: 270px;
        }
        .dc-sidenav-container {
            height: calc(100vh - 64px);
        }
        
        
        .dc-sidenav-container.fullscreen {
            height: 100vh;           
        }
`]
})
export class AppComponent implements OnInit {
    menu : MenuService;

    constructor(public route : ActivatedRoute,
                private router : Router,
                private dataService: DataService,
                public menuService: MenuService,
                public ls: LayoutService) {
        this.menu = menuService;
    };

    ngOnInit() {

    }


    sidenavMode() {
        if (this.ls.mobile) {
            return 'over';
        }

        return 'side';
    }

    updateConfig() {
        this.dataService.updateConfig();
    }
}
