import {MenuService} from "./menu.service";
import { Component, Inject } from '@angular/core';
import {  ActivatedRoute } from '@angular/router';


@Component({
    selector: 'devctrl-menu',
    template: `
<mat-nav-list>
    <ng-template ngFor let-item [ngForOf]="menu.menuItems()" [ngForTrackBy]="trackByName">
        <mat-list-item (click)="menu.go(item.route)">
            <a mat-line >{{item.name}}</a>
            <button mat-icon-button class="toggle-icon" 
                    [class.toggled]="item.isOpened" 
                    (click)="menu.toggleTopLevel($event, item)">
                <mat-icon>expand_more</mat-icon>
            </button>
        </mat-list-item>
        <a mat-list-item [hidden]="! item.isOpened"
                        *ngFor="let subitem of item.children" 
                        (click)="menu.go(subitem.route)">
            <span flex="5"></span>
            <p>{{subitem.name}}</p>
        </a>
        <mat-divider></mat-divider>
        <a mat-list-item (click)="menu.go(['devices'])">
            Devices
        </a>
        <mat-divider></mat-divider>
        <a mat-list-item (click)="menu.go(['config', 'data'])">
            Browse Data
        </a>
    </ng-template>
</mat-nav-list>
`,
    styles: [`
button.toggle-icon {
    display: block;
    margin-left: auto;
    vertical-align: middle;
    transform: rotate(180deg);
    transition: transform 0.3s ease-in-out;
}

button.toggle-icon.toggled {
    transform: rotate(0deg);
}
`]
})
export class MenuComponent {

    constructor(public menu: MenuService, private route : ActivatedRoute) {
        console.log("MenuComponent created");
    }

    trackByName(index: number, state) {
        return state.name;
    }
}