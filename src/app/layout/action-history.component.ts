import { OnInit, Component } from '@angular/core';
import {DataService} from "../data.service";

@Component({

    selector: 'devctrl-action-history',
    template: `
<div class="spacer" [class.closed]="closed">&nbsp;</div>
<div class="devctrl-card history-card" [class.closed]="closed">
    <mat-toolbar color="primary">
        <span class="text-subhead">Log</span>
        <button mat-icon-button (click)="toggle()">
            <mat-icon class="expand-icon">expand_more</mat-icon>
        </button>
    </mat-toolbar>
    <mat-list class="log-list">
        <mat-list-item *ngFor="let action of ds.logs;">
            <p mat-line class="message">{{action.name}}</p>
            <p mat-line class="ts">{{action.timestamp | date:'medium'}}</p>
        </mat-list-item>
        <mat-list-item *ngIf="ds.logs.length == 0">
            <p mat-line>Action log is empty</p>
        </mat-list-item>
    </mat-list>  
</div>
    `,
    styles: [`
    
.spacer.closed {
    flex: 1 1;
}
    
.history-card {
    width: 600px; 
}

.history-card.closed {
    flex: 0 0;
    width: 90px;
}

:host {
    max-width: 700px;
    display: flex;
    flex-direction: row;
    align-items: flex-end;
}

.expand-icon {
    transform: rotate(270deg);
}

.closed .expand-icon {
    transform: rotate(90deg);
}


.history-card.closed .log-list {
    visibility: hidden;
    height: 0px;
}

`]
})
export class ActionHistoryComponent implements OnInit {
    _open;

    constructor(public ds : DataService) { }

    ngOnInit() { }

    get closed() {
        return !this._open;
    }

    toggle() {
        this._open = ! this._open;
    }

}