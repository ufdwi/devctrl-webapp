import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent }   from './layout/app.component';
import { HttpClientModule } from '@angular/common/http';
import 'hammerjs';

import {MenuComponent} from "./layout/menu.component";
import {MenuService} from "./layout/menu.service";
import {ToolbarComponent} from "./layout/toolbar.component";

import {DataService} from "./data.service";
import {AppRoutingModule} from "./app-router.module";
import {EndpointsComponent} from "./endpoints/endpoints.component";
import {EndpointStatusComponent} from "./endpoints/endpoint-status.component";
import {EndpointComponent} from "./endpoints/endpoint.component";
import {ConfigComponent} from "./config.component";
import {ConfigDataComponent} from "./data-editor/config-data.component";
import {RecordComponent} from "./data-editor/record.component";
import {FkAutocompleteComponent} from "./data-editor/fk-autocomplete.component";
import {AlertDialog} from "./alert-dialog.component";
import {RecordEditorService} from "./data-editor/record-editor.service";
import {TableComponent} from "./data-editor/table.component";
import {FormsModule} from "@angular/forms";
import {ControlsModule} from "./controls/controls.module";
import {RoomComponent} from "./rooms/room.component";
import {RoomsComponent} from "./rooms/rooms.component";
import {PanelComponent} from "./rooms/panel.component";
import {ObjectEditorComponent} from "./data-editor/object-editor.component";
import {MediaService} from "./layout/media.service";
import {LayoutService} from "./layout/layout.service";
import {PanelDialogService} from "./rooms/panel-dialog.service";
import {ActionHistoryComponent} from "./layout/action-history.component";
import {DCMaterialModule} from "./dc-material.module";
import {ControlDetailComponent} from "./controls/control-detail.component";
import {WatcherActionValueComponent} from "./data-editor/watcher-action-value.component";
import { PanelGroupDialogComponent } from './rooms/panel-group-dialog/panel-group-dialog.component';
import {ClassroomModule} from "./classroom/classroom.module";
import {DevctrlCommonModule} from "./devctrl-common.module";
import {ImageDialogComponent} from "./image-dialog.component";
import {ControlIdSelectorComponent} from "./data-editor/control-id-selector";
import {ControlIdListSelectorComponent} from "./data-editor/control-id-list-selector/control-id-list-selector.component";


@NgModule({
    imports:      [
        BrowserModule,
        BrowserAnimationsModule,
        DCMaterialModule,
        HttpClientModule,
        AppRoutingModule,
        FormsModule,
        ClassroomModule,
        ControlsModule,
        DevctrlCommonModule
    ],
    declarations: [ MenuComponent,
        AlertDialog,
        AppComponent,
        ConfigComponent,
        ConfigDataComponent,
        ControlDetailComponent,
        ControlIdSelectorComponent,
      ControlIdListSelectorComponent,
        EndpointComponent,
        EndpointsComponent,
        EndpointStatusComponent,
        FkAutocompleteComponent,
        ImageDialogComponent,
        ObjectEditorComponent,
        PanelComponent,
        PanelGroupDialogComponent,
        RecordComponent,
        RoomComponent,
        RoomsComponent,
        TableComponent,
        ToolbarComponent,
        ActionHistoryComponent,
        WatcherActionValueComponent
        ],
    entryComponents: [
        AlertDialog,
        ImageDialogComponent,
        MenuComponent,
        PanelGroupDialogComponent,
        RecordComponent
    ],
    providers:    [
        DataService,
        MenuService,
        RecordEditorService,
        MediaService,
        PanelDialogService,
        LayoutService
    ],
    bootstrap:    [ AppComponent ]
})
export class AppModule {
    //ngDoBootstrap() {};
}
