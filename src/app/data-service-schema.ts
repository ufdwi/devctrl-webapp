

import {Control} from "@devctrl/common";
import {Endpoint} from "@devctrl/common";
import {OptionSet} from "@devctrl/common";
import {DCSerializable, IDCFieldDefinition, IDCTableDefinition} from "@devctrl/common";
import {ActionTrigger} from "@devctrl/common";
import {EndpointType} from "@devctrl/common";
import {Panel} from "@devctrl/common";
import {PanelControl} from "@devctrl/common";
import {Room} from "@devctrl/common";


export interface DSSchemaDefinition {
    [index: string] : IDCTableDefinition
}


let schema : DSSchemaDefinition = {};
schema[ActionTrigger.tableStr] = DCSerializable.typeTableDefinition(ActionTrigger);
schema[Control.tableStr] = DCSerializable.typeTableDefinition(Control);
schema[Endpoint.tableStr] = DCSerializable.typeTableDefinition(Endpoint);
schema[EndpointType.tableStr] = DCSerializable.typeTableDefinition(EndpointType);
schema[OptionSet.tableStr] = DCSerializable.typeTableDefinition(OptionSet);
schema[Panel.tableStr] = DCSerializable.typeTableDefinition(Panel);
schema[PanelControl.tableStr] = DCSerializable.typeTableDefinition(PanelControl);
schema[Room.tableStr] = DCSerializable.typeTableDefinition(Room);

export let dataServiceSchema = schema;

