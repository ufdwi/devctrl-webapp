import {Component, Input} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { MatDialogRef } from '@angular/material';


@Component({
    selector: 'devctrl-image-dialog',
    template: `
<div>
    <label>{{title}}</label>
    <div class="image">
        <img [src]="src" />
    </div>
    <button mat-button (click)="dialogRef.close()">{{ok}}</button>
<div>
`,
    //language=CSS
    styles: [`
        .image img {
            width: 50vw;
        }
        `]
})
export class ImageDialogComponent
{
    title;
    src;
    ok;

    constructor(public dialogRef: MatDialogRef<ImageDialogComponent>) {}


}