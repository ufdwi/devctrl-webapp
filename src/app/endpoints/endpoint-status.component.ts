import {Component, Input, OnInit, Injector} from '@angular/core';
import {IEndpointStatus, Endpoint} from "@devctrl/common";
import {DataService} from "../data.service";

@Component({
    selector: 'devctrl-endpoint-status',
    template: `
        <div class="status-row">
            <mat-icon [color]="statusIconColor('messengerConnected')"
                      [ngClass]="statusIconClasses('messengerConnected')"
                      [matTooltip]="statusTooltip('messengerConnected')">
                rowing
            </mat-icon>
            <mat-icon [color]="statusIconColor('reachable')"
                      [ngClass]="statusIconClasses('reachable')"
                      [matTooltip]="statusTooltip('reachable')">
                settings_ethernet
            </mat-icon>
            <mat-icon [color]="statusIconColor('connected')"
                      [ngClass]="statusIconClasses('connected')"
                      [matTooltip]="statusTooltip('connected')">
                link
            </mat-icon>
            <mat-icon [color]="statusIconColor('loggedIn')"
                      [ngClass]="statusIconClasses('loggedIn')"
                      [matTooltip]="statusTooltip('loggedIn')">
                perm_identity
            </mat-icon>
            <mat-icon [color]="statusIconColor('responsive')"
                      [ngClass]="statusIconClasses('responsive')"
                      [matTooltip]="statusTooltip('responsive')">
                 swap_vert
            </mat-icon>
            <mat-icon [color]="statusIconColor('ok')"
                      [ngClass]="statusIconClasses('ok')"
                      [matTooltip]="statusTooltip('ok')">
                done_all
            </mat-icon>
            <mat-checkbox class="md-primary"
                          [(ngModel)]="endpoint.enabled"
                          (change)="updateEnabled()"
                          (click)="stopPropagation($event)"
                          name="{{Enabled}}"
                          matTooltip="Enabled?">
            </mat-checkbox>
        </div>
`,
    //language=CSS
    styles: [`
.devctrl-icon-disabled {
    color: #bdbdbd;
}    
        .status-row {
            display: flex;
            flex-direction: row;
        }
        .dc-icon-hidden {
            visibility: hidden;
        }
        
        mat-checkbox {
            margin-left: 3px;
        }
`]

})
export class EndpointStatusComponent implements OnInit {
    endpoint : Endpoint;
    @Input() endpointId;
    @Input() backgroundColor;


    constructor(private dataService : DataService, private injector : Injector) {}

    ngOnInit() {
        this.endpoint = this.dataService.getRowRef('endpoints', this.endpointId) as Endpoint;
    }


    statusIconClasses (field: string) {
        let status = this.endpoint.epStatus[field];

        let classes = {};

        if (status) {

            if (this.backgroundColor != 'primary') {
                classes['md-primary'] = true;
            }
        }
        else {
            classes['devctrl-icon-disabled'] = true;
        }

        if (this.endpoint.epStatus.ok && field != 'ok') {
            // Hide sub-statuses when all is well
            classes['dc-icon-hidden'] = true;
        }


        return classes;
    }

    statusIconColor(field: string) : string {
        let status = this.endpoint.epStatus[field];

        if (status) {
            if (this.backgroundColor == 'primary') {
                return 'currentColor';
            }
            return 'primary';
        }
        else {
            return '';
        }
    }

    statusTooltip(field: string) : string {
        let status = this.endpoint.epStatus[field];

        if(status) {
            switch (field) {
                case 'ok' : {
                    return "Communicator OK";
                }
                case 'responsive' : {
                    return 'Responsive';
                }
                case 'loggedIn' : {
                    return 'Logged In';
                }
                case 'connected' : {
                    return 'Device Connected';
                }
                case 'reachable' : {
                    return 'PING OK';
                }
                case 'messengerConnected' : {
                    return 'Communicator Running';
                }
                default : {
                    return field;
                }
            }
        }

        switch(field) {
            case 'ok' : {
                return "Communicator Problem";
            }
            case 'responsive' : {
                return 'Not responsive';
            }
            case 'loggedIn' : {
                return 'Not Logged In';
            }
            case 'connected' : {
                return 'Not Connected';
            }
            case 'reachable' : {
                return 'Not Reachable';
            }
            case 'messengerConnected' : {
                return 'Communicator Disconnected';
            }
        }
    }

    stopPropagation(event) {
        event.stopPropagation();
    }

    updateEnabled() {
        console.log("update enabled called");
        this.dataService.updateEndpointEnabled(this.endpointId, this.endpoint.enabled);
    }
}