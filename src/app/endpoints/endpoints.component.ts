import {Component, Input, OnInit} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {IndexedDataSet} from "@devctrl/common";
import {Endpoint} from "@devctrl/common";
import {DataService} from "../data.service";
import {EndpointType} from "@devctrl/common";
import {MenuService} from "../layout/menu.service";
import {RecordEditorService} from "../data-editor/record-editor.service";

@Component({
    selector: 'devctrl-endpoints',
    template: `
<div id="devctrl-content-canvas">
    <div class="devctrl-card">
        <mat-toolbar color="primary">
            <div class="devctrl-toolbar-test">
                <button mat-button *devctrlAdminOnly (click)="addEndpoint($event)">Add Endpoint</button>
                <button mat-button *devctrlAdminOnly (click)="addEndpointType($event)">Add Endpoint Type</button>
            </div>
        </mat-toolbar>
        <mat-nav-list>
            <ng-template ngFor let-endpoint [ngForOf]="endpointsList">
                 <a mat-list-item 
                    [class.endpoint-disabled]="!endpoint.epStatus.ok"
                    (click)="menu.go(['devices', endpoint._id])">
                    {{endpoint.name}}
                    <span class="devctrl-spacer"></span>
                    <devctrl-endpoint-status [endpointId]="endpoint._id"></devctrl-endpoint-status>
                    <mat-icon>chevron_right</mat-icon>
                </a>
                <mat-divider></mat-divider>           
            </ng-template>       
        </mat-nav-list>
</div>
`,
    //language=CSS
    styles: [`
        .devctrl-card {
            max-width: 900px;
            flex: 1 1;
        }
        .endpoint-disabled {
            color: #bdbdbd;
        }
    `]
})
export class EndpointsComponent implements OnInit {
    endpoints : IndexedDataSet<Endpoint>;
    endpointsList : Endpoint[];


    constructor(private dataService : DataService,
                public route : ActivatedRoute,
                private menu : MenuService,
                private recordService : RecordEditorService){}

    ngOnInit() {
        this.endpoints = (<IndexedDataSet<Endpoint>>this.dataService.getTable(Endpoint.tableStr));
        this.endpointsList = this.dataService.sortedArray('endpoints', 'name') as Endpoint[];
        this.menu.currentTopLevel = MenuService.TOPLEVEL_DEVICES;
        this.menu.pageTitle = "Devices";
        this.menu.parentName = "";
    }

    addEndpoint($event) {
        this.recordService.editRecord($event, '0', Endpoint.tableStr);
    }

    addEndpointType($event) {
        this.recordService.editRecord($event, '0', EndpointType.tableStr);
    }
}