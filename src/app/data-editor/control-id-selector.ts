import {Input, Component, EventEmitter, Output, OnInit} from "@angular/core";
import {DataService} from "../data.service";
import {Observable, BehaviorSubject} from "rxjs";
import {Control} from "@devctrl/common";
import {ControlService} from "../controls/control.service";
import {map} from "rxjs/operators";
import {IndexedDataSet} from "../../../../common";

@Component({
  selector: 'devctrl-control-id-selector',
  template: `
<form class="control-id-form">
<mat-form-field>
<input type="text" matInput
name="fcValue"
  [placeholder]="name"
  [ngModel]="initialValue()"
(ngModelChange)="updateValue($event, key)"
  [matAutocomplete]="fc">
</mat-form-field>
<mat-autocomplete #fc="matAutocomplete" [displayWith]="displayValue">
<mat-option *ngFor="let fObj of filteredOptions | async" [value]="fObj">{{fObj.fkSelectName()}}</mat-option>
</mat-autocomplete>
</form>    
  `,
  //language=CSS
  styles: [`
    mat-form-field {
      width: 370px;
    }
    `]
})
export class ControlIdSelectorComponent implements OnInit {
  filteredOptions: Observable<Control[]>;
  inputUpdates = new BehaviorSubject<string>('');
  controls: Control[];
  @Input() name: string;
  @Input() controlId: string;
  @Output() onUpdate = new EventEmitter<string>();

  constructor( private ds: DataService) {}

  ngOnInit() {
    this.controls = <Control[]>this.ds.sortedArray(Control.tableStr);
    this.filteredOptions = this.inputUpdates.pipe(
      map((input, index) => {
        if (input.length <= 1) { return [] }
        const val = input.toLowerCase();
        let matches = [];
        let stLowerParts = val.split(' ');
        for (let item of this.controls) {
          let fkNameLower = item.fkSelectName().toLowerCase();

          let matched = true;
          for (let part of stLowerParts) {
            if (fkNameLower.indexOf(part) == -1) {
              matched = false;
              break;
            }
          }

          if (matched) {
            matches.push(item);
          }
        }

        return matches;
      }
    ));
  }

  displayValue(obj) {
    if (typeof obj === 'string') {
      return obj;
    }
    if (obj) { return obj.fkSelectName(); }
    return '';
  }


  initialValue() {
    if (this.controlId) {
      return this.controls.find(c => c.id == this.controlId).fkSelectName();
    }

    return '';
  }

  updateValue(obj: Control | string) {
    if (typeof obj === 'string') {
      console.log(`input value updated: ${obj} entered`);
      this.inputUpdates.next(obj);
    } else {
      console.log(`input value updated: object ${obj.name} selected`);
      this.onUpdate.emit(obj.id);
    }
  }
}

