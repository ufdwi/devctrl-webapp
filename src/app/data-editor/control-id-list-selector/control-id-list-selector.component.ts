import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'devctrl-control-id-list-selector',
  templateUrl: './control-id-list-selector.component.html',
  styleUrls: ['./control-id-list-selector.component.css']
})
export class ControlIdListSelectorComponent implements OnInit {

  @Input() name: string;
  @Input() controlIds: string[];
  @Output() onUpdate = new EventEmitter<string[]>();

  constructor() { }

  ngOnInit() {
  }

  addControl() {
    this.controlIds.push("");
  }

  controlUpdated(id, idx) {
    this.controlIds[idx] = id;
    this.onUpdate.emit(this.controlIds);
  }

  deleteControl(idx) {
    this.controlIds.splice(idx, 1);
    this.onUpdate.emit(this.controlIds);
  }
}
