import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ControlIdListSelectorComponent } from './control-id-list-selector.component';

describe('ControlIdListSelectorComponent', () => {
  let component: ControlIdListSelectorComponent;
  let fixture: ComponentFixture<ControlIdListSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ControlIdListSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ControlIdListSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
