import {Component, Input, Output, EventEmitter} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'devctrl-object-editor',
  template: `
<div>
    <span class="text-menu">
        {{keyPath()}}
    </span>
    <div style="margin-left: 24px;">
        <ng-template ngFor let-key [ngForOf]="keys()">
            <div class="prop-row" *ngIf="valueType(object[key]) != 'object'">
                <mat-form-field *ngIf="fieldType(key) === 'text'">
                    <input  matInput 
                            [placeholder]="key" 
                            [ngModel]="textValue(object[key])" 
                            (change)="updateValue($event.target.value, key)">
                </mat-form-field>
                <devctrl-control-id-selector *ngIf="fieldType(key) === 'control'"
                    [name]="key"
                    [controlId]="object[key]"
                    (onUpdate)="updateValue($event, key)">    
                </devctrl-control-id-selector>
                <devctrl-control-id-list-selector *ngIf="fieldType(key) === 'controlList'"
                    [name]="key"
                    [controlIds]="object[key]"
                    (onUpdate)="updateValue($event, key)">
                </devctrl-control-id-list-selector>
                <button type="button" mat-icon-button (click)="deleteValue(key)">
                    <mat-icon>delete</mat-icon>
                </button>
            </div>
            <devctrl-object-editor *ngIf="valueType(object[key]) == 'object'"
                            [object]="object[key]"
                            [fname]="key"
                            [pathPrefix]="keyPath()"
                            (onUpdate)="updateItem($event)">
            </devctrl-object-editor>
        </ng-template>
        <div class="new-prop-row">
            <mat-form-field>
                <input  matInput [placeholder]="newKeyPlaceholder()" [(ngModel)]="newKey" name="new-key">
            </mat-form-field>
            <mat-form-field>
                <input  matInput placeholder="Value" [(ngModel)]="newVal" name="new-val">
            </mat-form-field>
            <button mat-icon-button
                    type="button" 
                    (click)="addItem()"
                    color="primary">
                <mat-icon>add</mat-icon>
            </button>
        </div>
    </div>
</div>    
`,
  //language=CSS
  styles: [`
    devctrl-object-editor {
      flex: 1 1;
    }

    .new-prop-row {
      display: flex;
      flex-direction: row;
    }

    .prop-row {
      display: flex;
      flex-direction: row;
    }

    .prop-row mat-form-field {
      width: 370px;
    }
  `]
})
export class ObjectEditorComponent
{
  @Input() object;
  @Input() fname;
  @Input() pathPrefix;
  @Output() onUpdate = new EventEmitter<any>();
  newKey;
  newVal;

  constructor() {}


  addItem() {
    if (this.newKey && this.newVal) {
      let tempVal = '';
      try {
        tempVal = JSON.parse(this.newVal);
      }
      catch(e) {
        tempVal = this.newVal;
      }

      if (typeof this.object == 'undefined') {
        this.object = {};
      }

      this.object[this.newKey] = tempVal;
    }

    this.newKey = undefined;
    this.newVal = undefined;
    //angular.element(document).find('#oe-new-key').focus();

    this.onUpdate.emit({value: this.object, name: this.fname});
  }

  deleteValue(key) {
    delete this.object[key];
    this.onUpdate.emit({value: this.object, name: this.fname});
  }

  fieldType(key) {
    if (key.substring(key.length - 9) === "ControlId") return "control";
    if (key.substring(key.length - 10) === "ControlIds") return "controlList";
    return "text";
  }

  keys() {
    return Object.keys(this.object);
  }

  keyPath(key = "") {
    let path = this.fname;

    if (key) {
      path = `${path}.${key}`;
    }

    if (this.pathPrefix) {
      path = `${this.pathPrefix}.${path}`;
    }

    return path;
  }

  newKeyPlaceholder() {
    let path = this.keyPath('');
    return `New ${path} key`;
  }

  textValue(value) {
    if (Array.isArray(value)) {
      return JSON.stringify(value);
    }

    return value;
  }

  updateValue(val, key) {
    let tempVal = '';
    try {
      tempVal = JSON.parse(val);
    }
    catch(e) {
      tempVal = val;
    }

    this.object[key] = tempVal;
  }

  updateItem() {
    this.onUpdate.emit({value: this.object, name: this.fname});
  }

  valueType(value) {
    if (Array.isArray(value)) {
      return "array";
    }

    return typeof value;
  }
}