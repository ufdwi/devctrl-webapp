import {Component, Input, OnInit} from '@angular/core';
import {MenuService} from "../layout/menu.service";
import {PanelControl} from "@devctrl/common";
import {ControlService} from "./control.service";

@Component({

    selector: 'devctrl-ctrl',
    providers: [ControlService],
    templateUrl: './control.html',

    //language=CSS
    styles: [`        
.control-wrapper {
    flex: 1 1;
    width: 100%;
}

.devctrl-ctrl-item {
    min-height: 48px;
    width: 100%;
    display: flex;
    flex-direction: row;
}

.devctrl-ctrl-admin-placeholder {
    width: 48px;
}

:host {
    width: 100%;
}

`]

})
export class ControlComponent implements OnInit {
    @Input()panelControl: PanelControl;
    @Input()controlId;
    @Input()control;

    constructor(public cs: ControlService,
                public menu: MenuService) {}

    ngOnInit() {
        if (this.panelControl) {
            this.cs.panelControl = this.panelControl;
        }
        else if (this.control) {
            this.cs.control = this.control;
        }
        else {
            this.cs.controlId = this.controlId;
        }
    }



}
