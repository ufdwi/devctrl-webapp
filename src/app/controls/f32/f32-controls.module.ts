import {DCMaterialModule} from "../../dc-material.module";
import {FormsModule} from "@angular/forms";
import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {F32MultibuttonControl} from "./f32-multibutton.control";


@NgModule({
    imports:      [
        DCMaterialModule,
        FormsModule,
        CommonModule
    ],
    declarations: [
        F32MultibuttonControl
    ],
    exports: [
        F32MultibuttonControl
    ]
})
export class F32ControlsModule {}
