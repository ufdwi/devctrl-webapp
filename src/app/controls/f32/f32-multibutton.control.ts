import { Component, OnInit } from '@angular/core';
import { ControlService } from '../control.service';

@Component({

    selector: 'ctrl-f32-multibutton',
    template: `
<div class="devctrl-ctrl devctrl-ctrl-flex-layout">
    <label class="text-menu devctrl-ctrl-label">{{cs.name}}</label>
    <div *ngIf="cs.config('direction') !== 'reverse'">
        <button  mat-icon-button (click)="cs.setValue(1)">
            <mat-icon>play_arrow</mat-icon>
        </button>
        <button  mat-icon-button (click)="cs.setValue(2)">
            <mat-icon>fast_forward</mat-icon>
        </button>
        <button mat-icon-button (click)="cs.setValue(3)">
            <img src="/assets/images/icon_triple_fast.svg" />
        </button>
    </div>
    <div *ngIf="cs.config('direction') == 'reverse'">
        <button  mat-icon-button (click)="cs.setValue(1)">
            <mat-icon  class="rot180">play_arrow</mat-icon>
        </button>
        <button  mat-icon-button (click)="cs.setValue(2)">
            <mat-icon>fast_rewind</mat-icon>
        </button>
        <button  mat-icon-button (click)="cs.setValue(3)">
            <img class="rot180" src="/assets/images/icon_triple_fast.svg" />
        </button>
    </div>
</div>    
    `,
    styles: [`
.rot180 {
    transform: rotate(180deg);
}
`]
})
export class F32MultibuttonControl implements OnInit {
    constructor(public cs: ControlService) { }

    ngOnInit() { }
}