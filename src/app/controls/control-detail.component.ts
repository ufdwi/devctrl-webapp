import { Component, OnInit } from '@angular/core';
import {Control} from "@devctrl/common";
import {DataService} from "../data.service";
import {MenuService} from "../layout/menu.service";
import {Router, ActivatedRoute} from '@angular/router';
import {IndexedDataSet} from "@devctrl/common";
import {RecordEditorService} from "../data-editor/record-editor.service";
import {ActionTrigger} from "@devctrl/common";

@Component({

    selector: 'devctrl-ctrl-detail',
    //language=HTML
    template: `
<div id="devctrl-content-canvas">
    <div class="devctrl-card">
        <mat-tab-group #tabgroup>
            <mat-tab label="Overview">
                <mat-list>
                    <h3 mat-subheader>Control</h3>
                    <mat-list-item class="devctrl-ctrl-list-item">
                        <devctrl-ctrl [controlId]="control.id"></devctrl-ctrl>
                    </mat-list-item>
                </mat-list>
                <h3 class="settings-subheader">
                    Settings 
                    <button mat-icon-button (click)="openRecordEditor($event)"><mat-icon>create</mat-icon></button>
                </h3>
                
                <form>
                    <div class="settings-container">
                        <fk-autocomplete [object]="control"
                            [field]="control.fieldDefinitions[1]"
                            (onUpdate)="controlUpdated($event)">    
                        </fk-autocomplete>
                        <mat-form-field>
                            <input  matInput 
                                placeholder="Name"
                                name="name"
                                [(ngModel)]="control.name"> 
                        </mat-form-field>
                        <mat-form-field>
                            <input  matInput disabled
                                placeholder="CTID"
                                name="ctid"
                                [(ngModel)]="control.ctid">
                        </mat-form-field>
                        <mat-select [placeholder]="'UI Type'"
                            [(ngModel)]="control.usertype"
                            name="usertype">
                            <mat-option [value]="option.value"
                                       *ngFor="let option of control.ownFields[2].options">
                                {{option.name}}
                            </mat-option>                            
                        </mat-select>
                        <mat-select [placeholder]="'Control Type'"
                            [(ngModel)]="control.control_type"
                            name="control_type">
                            <mat-option [value]="option.value"
                                       *ngFor="let option of control.ownFields[3].options">
                                {{option.name}}
                            </mat-option>                            
                        </mat-select>
                        <mat-checkbox
                                  [(ngModel)]="control.poll"
                                  name="poll">
                            Poll?
                        </mat-checkbox>
                        <mat-form-field>
                            <input  matInput
                                placeholder="Value"
                                name="value"
                                [(ngModel)]="control.value">
                        </mat-form-field>
                    </div>        
                </form>     
            </mat-tab>
            <mat-tab [label]="triggerLabel()">
                <mat-list>
                    <div class="action-subhead">
                        <h3 mat-subheader>Rules triggered by this control</h3>
                        <button mat-icon-button 
                                (click)="addTrigger($event)"
                                color="primary"
                                tooltip="Add action">
                            <mat-icon>add</mat-icon>
                        </button>
                    </div>
                    <ng-template ngFor let-actionTrigger [ngForOf]="triggeredList()">
                        <mat-list-item>
                            <h3 mat-line class="triggered-detail">
                                {{actionTrigger.action_control.endpoint.name}}
                                :&nbsp;
                                {{actionTrigger.action_control.name}}
                            </h3>
                            <div mat-line>
                                {{actionTrigger.valueDescription}}
                            </div>
                            <button mat-icon-button (click)="editWatcherRule($event, actionTrigger)">
                                <mat-icon>create</mat-icon>
                            </button>
                        </mat-list-item>
                    </ng-template>
                    <div class="action-subhead">
                        <h3 mat-subheader>Rules modifying this control</h3>
                        <button mat-icon-button
                                (click)="addAction($event)"
                                color="primary"
                                tooltip="Add action">
                            <mat-icon>add</mat-icon>
                        </button>
                    </div>
                    <ng-template ngFor let-actionTrigger [ngForOf]="actionList()">
                        <mat-list-item>
                            <h3 mat-line class="triggered-detail">
                                {{actionTrigger.trigger_control.endpoint.name}}
                                :&nbsp;
                                {{actionTrigger.trigger_control.name}}
                            </h3>
                            <div mat-line>
                                {{actionTrigger.valueDescription}}
                            </div>
                            <button mat-icon-button (click)="editWatcherRule($event, actionTrigger)">
                                <mat-icon>create</mat-icon>
                            </button>
                        </mat-list-item>
                    </ng-template>
                </mat-list>
            </mat-tab>
            <mat-tab label="Log">
            </mat-tab>
        </mat-tab-group>
    </div>
</div>
    `,
    //language=CSS
    styles: [`
        .action-subhead {
            display: flex;
            flex-direction: row;
        }
        .devctrl-card {
            max-width: 1600px;
            flex: 1 1;
        }
        .settings-container {
            display: flex;
            flex-direction: column;
            padding-right: 16px;
            padding-left: 16px;
        }
        .settings-subheader {
            display: block;
            box-sizing: border-box;
            height: 64px;
            padding: 16px;
            margin: 0;
            font-size: 14px;
            font-weight: 500;
            color: rgba(0,0,0,.54);
        }
        .triggered-detail {
            display: flex;
            flex-direction: row;
        }
        .triggered-field {
            flex: 1 1;
        }
        mat-select {
            margin-top: 24px;
        }
        mat-checkbox {
            margin-top: 10px;
            margin-bottom: 10px;
        }
    `]
})
export class ControlDetailComponent implements OnInit {
    control : Control;
    actionTriggers : IndexedDataSet<ActionTrigger>;
    constructor(private route : ActivatedRoute,
                private ds: DataService,
                private recordService: RecordEditorService,
                private menu : MenuService,) { }

    ngOnInit() {
        this.actionTriggers = this.ds.getTable(ActionTrigger.tableStr) as IndexedDataSet<ActionTrigger>;
        this.route.data.subscribe((data: { control: Control }) => {
            this.menu.currentTopLevel = MenuService.TOPLEVEL_DEVICES;
            this.control = data.control;
            console.log(`control ${this.control.id} loaded`);
            if (this.control) {
                this.menu.pageTitle = this.control.name;
                this.menu.parentName = this.control.endpoint.name;
                this.menu.parentRoute = ['devices', this.control.endpoint.id];
            }
            //this.watcherRules = this.control.referenced[ActionTrigger.tableStr] as IndexedDataSet<ActionTrigger>;
        });
    }

    actionList() {
        if (this.actionTriggers) {
            return Object.keys(this.actionTriggers)
                .map(id => this.actionTriggers[id])
                .filter(rule => { return rule.action_control_id == this.control.id});
        }
        return [];
    }

    /**
     * Add a new ActionTrigger, with this control as the action_control
     */

    addAction($event) {
        this.recordService.addRecord($event, ActionTrigger.tableStr,
            {
                action_control: this.control,
                enabled: true
            });
    }

    /**
     * Add a new ActionTrigger, with this control as the trigger_control
     * @param update
     */

    addTrigger($event) {
        this.recordService.addRecord($event, ActionTrigger.tableStr,
            {
                trigger_control: this.control,
                enabled: true
            });
    }

    controlUpdated(update) {
        this.control[update.name] = update.value;
    }

    editWatcherRule($event, actionTrigger) {
        this.recordService.editRecord($event, actionTrigger.id, actionTrigger.table);
    }

    openRecordEditor($event) {
        this.recordService.editRecord($event, this.control.id, this.control.table);
    }

    triggeredList() {
        if (this.actionTriggers) {
            return Object.keys(this.actionTriggers)
                .map(id => this.actionTriggers[id])
                .filter(rule => { return rule.trigger_control_id == this.control.id});
        }
        return [];
    }



    triggerLabel() {
        let ruleCount = this.actionList().length + this.triggeredList().length;


        return `Action Triggers (${ruleCount})`;
    }
}