import { Injectable } from '@angular/core';
import {DataService} from "../data.service";
import {RecordEditorService} from "../data-editor/record-editor.service";
import {PanelControl} from "@devctrl/common";
import {Control} from "@devctrl/common";
import {ActionTrigger} from "@devctrl/common";
import {Router} from '@angular/router';
import {IndexedDataSet} from "@devctrl/common";

@Injectable()
export class ControlService {
    private _panelControl : PanelControl;
    private _control : Control;
    private _siblings : { [ctid : string]: Control } = {};
    components : { [name: string] : Control} = {};
    panelContext = false;

    constructor(public dataService : DataService,
                public recordService : RecordEditorService,
                private router: Router) {}


    get panelControl() : PanelControl {
        return this._panelControl;
    }

    set panelControl(pc: PanelControl) {
        this.panelContext = true;
        this._panelControl = pc;
        this._control = pc.control;
    }


    get control(): Control {
        return this._control;
    }

    set control(val: Control) {
        this._control = val;
        if (this._panelControl && this._panelControl.control_id != val._id) {
            this._panelControl = undefined;
            this.panelContext = false;
        }
    }

    get controlId() {
        if (! this._control) return;
        return this._control._id;
    }

    set controlId(id: string) {
        this.control = this.dataService.getRowRef(Control.tableStr, id) as Control;
    }

    get disabled() {
        return ! this.enabled;
    }

    get enabled() {
        if (! this._control) return false;
        return this._control.endpoint.epStatus.ok;
    }

    get type() : string {
        return this._control.usertype;
    }

    get name() {
        if (this.panelContext && this._panelControl.name) {
            return this._panelControl.name;
        }

        return this._control.name;
    }

    get value() {
        if (! this._control) return;
        return this._control.value;
    }

    set value(val) {
        if (! this._control) return;
        this._control.value = val;
    }


    addWatcherRule($event) {
        this.recordService.editRecord($event, '0', ActionTrigger.tableStr,
            { watched_control_id : this.control._id});
    }

    config(key, component = "") {
        if (component) {
            if (this.components[component]
                && typeof this.components[component].config == 'object'
                && this.components[component].config[key]) {
                return this.components[component].config[key];
            }
            return '';
        }

        if (typeof this.control.config == 'object' && this.control.config[key]) {
            return this.control.config[key];
        }

        return '';
    }

    editControl($event) {
        //this.recordService.editRecord($event, this.control._id, this.control.table);
        this.router.navigate(['/controls', this.control.id]);
    }

    editOptions($event) {
        if (this.control.option_set) {
            this.recordService.editRecord(
                $event,
                this.control.option_set_id,
                this.control.option_set.table);
        }
        else {
            this.recordService.editRecord(
                $event,
                this.control._id,
                this.control.table
            );
        }
    }

    editPanelControl($event) {
        this.recordService.editRecord($event, this.panelControl._id, this.panelControl.table);
    }

    findSiblingByCtid(ctid: string) {
        if (this._siblings[ctid]) {
            return this._siblings[ctid];
        }

        let controls = this.control.endpoint.referenced.controls as IndexedDataSet<Control>;
        for (let id of Object.keys(controls)) {
            if (controls[id].ctid == ctid) {
                this._siblings[ctid] = controls[id];
                return controls[id];
            }
        }

        throw new Error(`sibling control not located: ${ctid}`);
    }



    floatConfig(key, defVal : number = 0, component = "") {
        if (component) {
            if (this.components[component]
                && typeof this.components[component].config == 'object'
                && typeof this.components[component].config[key] != 'undefined') {
                return parseFloat(this.components[component].config[key]);
            }

            return defVal;
        }

        if (typeof this.control.config !== 'object' ||
            typeof this.control.config[key] == 'undefined') {
            return defVal;
        }

        return parseFloat(this.control.config[key]);
    }

    getControl(id: string): Control {
        return this.dataService.getRowRef(Control.tableStr, id) as Control;
    }

    intConfig(key, component = "") {
        if (component) {
            if (this.components[component].config[key]) {
                return parseInt(this.components[component].config[key]);
            }
            else {
                return 0;
            }
        }
        if (this.config(key)) {
            return parseInt(this.config(key));
        }

        return 0;
    }


    loadComponentControls() {
        let componentConfig = this.control.config.componentControls;
        this.components = {};

        if (! componentConfig) {
            return {};
        }


        for (let component of Object.keys(componentConfig)) {
            this.components[component] = this.findSiblingByCtid(componentConfig[component]);
        }

        return this.components;
    }

    selectMenuItem(val) {
        this.setValue(val);
    }

    selectOptions() {
        if (this.control.config.optionsCtid) {
            // Use the value of specified control as the options set
            // This logic is here instead of in the control because it requires access to the DataService
            let optionsControl = this.dataService.getControlByCtid(this.control.config.optionsCtid);
            if (optionsControl) {
                return optionsControl.value;
            }
        }
        return this.control.selectOptions();
    }

    selectOptionsArray() {
        let options = this.selectOptions();

        if (options[0] && typeof options[0].value !== 'undefined') {
            // Options are already in appropriate array form
            return options;
        }

        let optionsArray = Object.keys(options).map( value => {
            return { name: options[value], value: value };
        });

        return optionsArray;
    }

    setValue(val, id = "") {
        if (! id) {
            this.value = val;
        }
        else {
            this.getControl(id).value = val;
        }
        this.updateValue(id);
    }

    selectValueName() {
        return this.control.selectValueName();
    }


    trackByValue(idx, obj) {
        return obj.value;
    }

    updateComponentValue(componentName : string) {
        if (this.components[componentName]) {
            let control = this.components[componentName];
            this.dataService.updateControlValue(control);
            this.dataService.logAction(
                `Control update requested: set ${control.fkSelectName()} to ${control.value}`,
                ['control update requested'],
                [control._id, control.endpoint_id]);
        }
    }

    updateValue(id : string = "") {
        let control = this.control;
        if (id) {
            control = this.getControl(id);
        }

        this.dataService.updateControlValue(control);
        this.dataService.logAction(
            `Control update requested: set ${control.fkSelectName()} to ${control.value}`,
            ['control update requested'],
            [control._id, control.endpoint_id]);
    }
}