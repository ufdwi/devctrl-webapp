import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Control} from "@devctrl/common";
import {ControlService} from "./control.service";

@Component({

    selector: 'devctrl-control-value-selector',
    providers: [ControlService],
    template: `
<div class="value-selector-switch">
  <mat-form-field  [ngSwitch]="control.usertype">
        <mat-select *ngSwitchCase="'select'"
                [ngModel]="controlValue"
                name="select"
                [placeholder]="placeholder"
                (selectionChange)="valueChange($event.value)">
            <mat-option [value]="obj.value" *ngFor="let obj of cs.selectOptionsArray(); trackBy: trackByValue">
                {{obj.name}}
            </mat-option>    
        </mat-select>
        <mat-slide-toggle *ngSwitchCase="'switch'"
            [ngModel]="controlValue"
            (change)="valueChange($event)"></mat-slide-toggle>
      
        <input  *ngSwitchDefault
         matInput name="value"  
                [placeholder]="placeholder"
                [ngModel]="controlValue"
                (change)="valueChange($event)">
   </mat-form-field>
</div>
    `,
    //language=CSS
    styles: [`        
        mat-select {
            width: 100%;
        }
        
        .value-selector-switch {
            width: 100%;
        }
    `]
})
export class ControlValueSelectorComponent implements OnInit {

    @Input() controlValue;
    @Input() placeholder : string;
    @Output() controlValueChange = new EventEmitter<any>();

    constructor(public cs : ControlService) { }

    ngOnInit() { }

    @Input() set control(control : Control) {
        this.cs.control = control;
    }

    get control() {
        return this.cs.control;
    }


    trackByValue(idx, obj) {
        return obj.value;
    }

    valueChange(value) {
        this.controlValueChange.emit(value);
    }
}