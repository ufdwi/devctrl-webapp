import {FormsModule} from "@angular/forms";
import {NgModule} from "@angular/core";
import {CommonModule, DecimalPipe} from "@angular/common";
import {CLQLFaderControl} from "./clql-fader.control";
import {CLQLFaderComboControl} from "./clql-fader-combo.control";
import {DCMaterialModule} from "../../dc-material.module";




@NgModule({
    imports:      [
        DCMaterialModule,
        FormsModule,
        CommonModule
    ],
    declarations: [
        CLQLFaderControl,
        CLQLFaderComboControl
    ],
    exports: [
        CLQLFaderControl,
        CLQLFaderComboControl
    ],
    providers: [
        DecimalPipe
    ]
})
export class YamahaControlsModule {}
