import {DCMaterialModule} from "../../dc-material.module";
import {FormsModule} from "@angular/forms";
import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {ButtonControl} from "./button.control";
import {DefaultControl} from "./default.control";
import {SwitchControl} from "./switch.control";
import {SliderControl} from "./slider.control";
import {SelectControl} from "./select.control";
import {SelectReadonlyControl} from "./select-readonly.control";
import {ButtonSetControl} from "./button-set.control";
import {LevelControl} from "./level.control";
import {ImageControl} from "./image.control";
import {Slider2dControl} from "./slder2d.control";
import {HyperlinkControl} from "./hyperlink.control";
import {SwitchReadonlyControl} from "./switch-readonly.control";
import {ReadonlyControl} from "./readonly.control";
import {TreeReadonlyControl} from "./tree-readonly";
import {ArrowButtonSetControl} from "./arrow-button-set";

@NgModule({
    imports:      [
        DCMaterialModule,
        FormsModule,
        CommonModule
    ],
    declarations: [
        ArrowButtonSetControl,
        ButtonControl,
        ButtonSetControl,
        DefaultControl,
        HyperlinkControl,
        ImageControl,
        LevelControl,
        ReadonlyControl,
        SelectControl,
        SelectReadonlyControl,
        SliderControl,
        Slider2dControl,
        SwitchControl,
        SwitchReadonlyControl,
        TreeReadonlyControl
    ],
    exports: [
        ArrowButtonSetControl,
        ButtonControl,
        ButtonSetControl,
        DefaultControl,
        HyperlinkControl,
        ImageControl,
        LevelControl,
        ReadonlyControl,
        SelectControl,
        SelectReadonlyControl,
        SliderControl,
        Slider2dControl,
        SwitchControl,
        SwitchReadonlyControl,
        TreeReadonlyControl
    ]
})
export class BasicControlsModule {}
