import { Component, OnInit } from '@angular/core';
import { ControlService } from '../control.service';

@Component({

    selector: 'ctrl-arrow-button-set',
    template: `
<div class="devctrl-ctrl devctrl-ctrl-flex-layout">
    <label class="text-menu devctrl-ctrl-label">{{cs.name}}</label>
    <div >
        <button  mat-icon-button (click)="cs.setValue(buttonValue(1))">
            <mat-icon [style.transform]="buttonOrientation()">play_arrow</mat-icon>
        </button>
        <button  mat-icon-button (click)="cs.setValue(buttonValue(2))">
            <mat-icon [style.transform]="buttonOrientation()">fast_forward</mat-icon>
        </button>
        <button mat-icon-button (click)="cs.setValue(buttonValue(3))">
            <img [style.transform]="buttonOrientation()" src="/assets/images/icon_triple_fast.svg" />
        </button>
    </div>
</div>    
    `,
    styles: [`
`]
})
export class ArrowButtonSetControl implements OnInit {
    constructor(public cs: ControlService) { }

    ngOnInit() { }

    buttonOrientation() {
        let o = this.cs.config("buttonOrientation");

        if (o == "down") {
            return "rotate(90deg)";
        }
        else if (o == "left") {
            return "rotate(180deg)";
        }
        else if (o == "up") {
            return "rotate(270deg)";
        }
        else {
            return "rotate(0deg)";
        }

    }




    buttonValue(buttonNum: number) {
        let bVals;
        if (bVals = this.cs.config("buttonValues")) {
            return bVals[buttonNum];
        }

        return buttonNum;
    }


}