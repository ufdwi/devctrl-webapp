import { Component, OnInit } from '@angular/core';
import { ControlService } from '../control.service';

@Component({

    selector: 'ctrl-tree-readonly',
    template: `
<div class="devctrl-ctrl devctrl-ctrl-flex-layout">
    <label class="text-menu devctrl-ctrl-label">{{cs.name}}</label>
    <span>
        <div *ngFor="let node of nodes()">
            <div [class.level0]="node.level == 0"
                [class.level1]="node.level == 1">
                {{node.key}} : {{node.value}}
            </div>
        </div>
    </span>
</div>
    `,
    //language=CSS
    styles: [`        
        .level0 {
            margin-left: -40px;
        }
        .level1 { 
            margin-left: -20px;
        }
        .level2 {
            
        }
    `]
})
export class TreeReadonlyControl implements OnInit {
    constructor(public cs: ControlService) { }

    ngOnInit() { }

    nodes() {
        return this.nodeArray(this.cs.value, 0);
    }

    nodeArray(obj, level) {
        return Object.keys(obj).reduce((accumulator, key) => {
            const value = obj[key];
            const node = {
                key: key,
                value: value,
                level: level
            };

            accumulator = accumulator.concat(node);

            if (value != null) {
                if (typeof value === 'object') {
                    let children = this.nodeArray(value, level + 1);
                    accumulator = accumulator.concat(children);
                    node.value = '';
                }
            }

            return accumulator;
        }, []);
    }
}