import { Component, OnInit } from '@angular/core';
import { ControlService } from '../control.service';

@Component({

    selector: 'ctrl-slider',
    template: `
<div class="devctrl-ctrl">
    <label class="text-menu devctrl-ctrl-label">{{cs.name}}</label>
    <mat-slider style="flex: 3 1;"
               min="{{cs.intConfig('min')}}"
               max="{{cs.intConfig('max')}}"
               step="{{cs.floatConfig('step', 1)}}"
               [(ngModel)]="cs.value"
               (change)="cs.updateValue()"
                [disabled]="cs.disabled">
    </mat-slider>
    <mat-form-field>
        <input  matInput
               class="devctrl-slider-input"
               type="number"
                [disabled]="cs.disabled"
               [(ngModel)]="cs.value"
               (change)="cs.updateValue()">
    </mat-form-field>
</div>    
    `,
    styles: [`
.devctrl-ctrl {
    display: flex; 
    flex-direction: row; 
    align-items: center;
}

.devctrl-slider-input {
    width: 60px;
}

`]
})
export class SliderControl implements OnInit {
    constructor(public cs: ControlService) { }

    ngOnInit() { }
}